@echo off
rem For use with Windows
cd "nSide"

mkdir "obj"

mingw32-make -j6
if not exist "out\nSide.exe" (pause)

move "out\*.exe" ".."

cd ".."
cd "cart-pal"

mingw32-make -j6
if not exist "out\cart-pal.exe" (pause)
move "out\*.exe" ".."

@echo on