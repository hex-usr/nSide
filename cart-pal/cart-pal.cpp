#include <nall/nall.hpp>
using namespace nall;

#include <hiro/hiro.hpp>
using namespace hiro;

auto locate(string name) -> string {
  //Use icarus's paths, as cart-pal has no differences in configuration settings
  string location = {Path::program(), name};
  if(inode::exists(location)) return location;

  directory::create({Path::userData(), "icarus/"});
  return {Path::userData(), "icarus/", name};
}

#include "../icarus/settings.cpp"
IcarusSettings settings;

#include "../icarus/heuristics/heuristics.hpp"
#include "../icarus/heuristics/heuristics.cpp"
#include "heuristics/atari-2600.cpp"
#include "heuristics/famicom.cpp"
#include "../icarus/heuristics/sg-1000.cpp"
#include "../icarus/heuristics/sc-3000.cpp"
#include "../icarus/heuristics/master-system.cpp"
#include "../icarus/heuristics/mega-drive.cpp"
#include "../icarus/heuristics/pc-engine.cpp"
#include "../icarus/heuristics/supergrafx.cpp"
#include "../icarus/heuristics/colecovision.cpp"
#include "../icarus/heuristics/msx.cpp"
#include "../icarus/heuristics/game-boy.cpp"
#include "../icarus/heuristics/game-boy-advance.cpp"
#include "../icarus/heuristics/game-gear.cpp"
#include "../icarus/heuristics/wonderswan.cpp"
#include "../icarus/heuristics/neo-geo-pocket.cpp"
#include "../icarus/heuristics/neo-geo-pocket-color.cpp"
#include "heuristics/vs-system.cpp"
#include "../icarus/heuristics/bs-memory.cpp"
#include "../icarus/heuristics/sufami-turbo.cpp"

#include "core/core.hpp"
#include "core/core.cpp"
#include "core/atari-2600.cpp"
#include "core/famicom.cpp"
#include "../icarus/core/sg-1000.cpp"
#include "../icarus/core/sc-3000.cpp"
#include "../icarus/core/master-system.cpp"
#include "../icarus/core/mega-drive.cpp"
#include "../icarus/core/pc-engine.cpp"
#include "../icarus/core/supergrafx.cpp"
#include "../icarus/core/colecovision.cpp"
#include "../icarus/core/msx.cpp"
#include "../icarus/core/game-boy.cpp"
#include "../icarus/core/game-boy-color.cpp"
#include "../icarus/core/game-boy-advance.cpp"
#include "../icarus/core/game-gear.cpp"
#include "../icarus/core/wonderswan.cpp"
#include "../icarus/core/wonderswan-color.cpp"
#include "../icarus/core/neo-geo-pocket.cpp"
#include "../icarus/core/neo-geo-pocket-color.cpp"
#include "../icarus/core/pocket-challenge-v2.cpp"
#include "core/vs-system.cpp"
#include "core/playchoice-10.cpp"
#include "core/famicombox.cpp"
#include "../icarus/core/bs-memory.cpp"
#include "../icarus/core/sufami-turbo.cpp"
#include "core/deprecated.cpp"

#if !defined(ICARUS_LIBRARY)

CartPal cart_pal;
#include "../icarus/ui/ui.hpp"
#include "ui/scan-dialog.cpp"
#include "../icarus/ui/settings-dialog.cpp"
#include "ui/import-dialog.cpp"
#include "../icarus/ui/error-dialog.cpp"

auto hiro::initialize() -> void {
  Application::setName("cart-pal");
}

#include <nall/main.hpp>
auto nall::main(Arguments arguments) -> void {
  if(arguments.size() == 1 && arguments[0] == "--name") {
    return print("cart-pal");
  }

  if(arguments.size() == 2 && arguments[0] == "--manifest" && directory::exists(arguments[1])) {
    return print(cart_pal.manifest(arguments[1]));
  }

  if(arguments.size() == 2 && arguments[0] == "--import" && file::exists(arguments[1])) {
    if(string target = cart_pal.import(arguments[1])) {
      return print(target, "\n");
    }
    return;
  }

  if(arguments.size() == 1 && arguments[0] == "--import") {
    if(string source = BrowserDialog()
    .setTitle("Load ROM File")
    .setPath(settings["icarus/Path"].text())
    .setFilters("ROM Files|"
      "*.a26:"
      "*.fc:*.nes:"
      "*.sg1000:*.sg:"
      "*.sc3000:*.sc:"
      "*.ms:*.sms:"
      "*.md:*.smd:*.gen:"
      "*.pce:"
      "*.sgx:"
      "*.cv:*.col:"
      "*.msx:"
      "*.gb:"
      "*.gbc:"
      "*.gba:"
      "*.gg:"
      "*.ws:"
      "*.wsc:"
      "*.pc2:"
      "*.ngp:"
      "*.ngpc:*.ngc:"
      "*.vs:"
      "*.pc10:"
      "*.fcb:"
      "*.bs:"
      "*.st:"
      "*.zip"
    ).openFile()) {
      if(string target = cart_pal.import(source)) {
        settings["icarus/Path"].setValue(Location::path(source));
        return print(target, "\n");
      }
    }
    return;
  }

  new ScanDialog;
  new SettingsDialog;
  new ImportDialog;
  new ErrorDialog;

  if constexpr(platform() == Platform::MacOS) {
    Application::Cocoa::onAbout([&] {
      MessageDialog().setTitle("About cart-pal").setText({
        "cart-pal\n\n"
        "Based on icarus, the companion program to higan\n"
        "Famicom Additions Author: hex_usr\n"
        "License: GPLv3\n"
        "icarus Website: https://byuu.org/higan/\n"
      }).information();
    });
    Application::Cocoa::onPreferences([&] {
      scanDialog->settingsButton.doActivate();
    });
    Application::Cocoa::onQuit([&] {
      Application::quit();
    });
  }

  scanDialog->show();
  Application::run();
  settings.save();
}

#endif
