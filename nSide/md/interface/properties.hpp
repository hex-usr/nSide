struct Properties : Setting<> {
  struct CPU : Setting<> { using Setting::Setting;
    Setting<natural> version{this, "version", 0};
  } cpu{this, "cpu"};

  struct PSG : Setting<> { using Setting::Setting;
    Setting<natural> volume{this, "volume", 0x1400};
  } psg{this, "psg"};

  struct Memory : Setting<> { using Setting::Setting;
    Setting<string> type{this, "type", "ROM"};
    Setting<natural> size{this, "size", 2048};
    Setting<string> content{this, "content", "Boot"};
  } memory{this, "memory"};

  Properties() : Setting{"system"} {
    cpu.version.setValid({0, 1});
    psg.volume.setValid({0x1000, 0x1400, 0x1800, 0x1c00, 0x2000});
  }
};

extern Properties property;
