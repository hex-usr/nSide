#include <md/md.hpp>

namespace higan::MegaDrive {

Options option;
Properties property;

auto MegaDriveInterface::information() -> Information {
  Information information;
  information.manufacturer = "Sega";
  information.name         = "Mega Drive";
  information.extension    = "md";
  information.resettable   = true;
  return information;
}

auto MegaDriveInterface::display() -> Display {
  Display display;
  display.type   = Display::Type::CRT;
  display.colors = 3 * (1 << 9);
  display.width  = 320;
  display.height = 240;
  display.internalWidth  = 1280;
  display.internalHeight =  480;
  display.aspectCorrection = 1.0;
  return display;
}

auto MegaDriveInterface::color(uint32 color) -> uint64 {
  uint R = color.bits(0, 2);
  uint G = color.bits(3, 5);
  uint B = color.bits(6, 8);
  uint M = color.bits(9,10);

  if(M == 0);                            //shadow
  if(M == 1) R <<= 1, G <<= 1, B <<= 1;  //normal
  if(M == 2) R  += 7, G  += 7, B  += 7;  //highlight

  uint64 r;
  uint64 g;
  uint64 b;

  if(option.video.colorEmulation()) {
    static const uint16 gammaRamp[15] = {
      0x0000, 0x1d12, 0x33af, 0x4642,
      0x5737, 0x65c1, 0x744a, 0x8204,
      0x908e, 0x9e48, 0xacd1, 0xbc29,
      0xcebc, 0xe48a, 0xffff
    };
    r = gammaRamp[R];
    g = gammaRamp[G];
    b = gammaRamp[B];
  } else {
    r = image::normalize(R, 4, 16) + image::normalize(R, 4, 16) / 14;
    g = image::normalize(G, 4, 16) + image::normalize(G, 4, 16) / 14;
    b = image::normalize(B, 4, 16) + image::normalize(B, 4, 16) / 14;
  }

  return r << 32 | g << 16 | b << 0;
}

auto MegaDriveInterface::loaded() -> bool {
  return system.loaded();
}

auto MegaDriveInterface::hashes() -> vector<string> {
  return cartridge.hashes();
}

auto MegaDriveInterface::manifests() -> vector<string> {
  return cartridge.manifests();
}

auto MegaDriveInterface::titles() -> vector<string> {
  return cartridge.titles();
}

auto MegaDriveInterface::load() -> bool {
  return system.load(this);
}

auto MegaDriveInterface::save() -> void {
  system.save();
}

auto MegaDriveInterface::unload() -> void {
  save();
  system.unload();
}

auto MegaDriveInterface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Extension,   "Extension Port"   }};
}

auto MegaDriveInterface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::None,        "None"         },
    {ID::Device::ControlPad,  "Control Pad"  },
    {ID::Device::FightingPad, "Fighting Pad" },
    {ID::Device::Mouse,       "Mouse"        },
    {ID::Device::SegaTap,     "Sega Tap"     },
    {ID::Device::EA4WayPlay,  "EA 4 Way Play"}
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::None,        "None"         },
    {ID::Device::ControlPad,  "Control Pad"  },
    {ID::Device::FightingPad, "Fighting Pad" },
    {ID::Device::Mouse,       "Mouse"        },
    {ID::Device::SegaTap,     "Sega Tap"     },
    {ID::Device::EA4WayPlay_, "EA 4 Way Play"}
  };

  if(port == ID::Port::Extension) return {
    {ID::Device::None, "None"}
  };

  return {};
}

auto MegaDriveInterface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::ControlPad) return {
    {Type::Hat,     "Up"   },
    {Type::Hat,     "Down" },
    {Type::Hat,     "Left" },
    {Type::Hat,     "Right"},
    {Type::Button,  "A"    },
    {Type::Button,  "B"    },
    {Type::Button,  "C"    },
    {Type::Control, "Start"}
  };

  if(device == ID::Device::FightingPad) return {
    {Type::Hat,     "Up"   },
    {Type::Hat,     "Down" },
    {Type::Hat,     "Left" },
    {Type::Hat,     "Right"},
    {Type::Button,  "A"    },
    {Type::Button,  "B"    },
    {Type::Button,  "C"    },
    {Type::Button,  "X"    },
    {Type::Button,  "Y"    },
    {Type::Button,  "Z"    },
    {Type::Control, "Mode" },
    {Type::Control, "Start"}
  };

  if(device == ID::Device::Mouse) return {
    {Type::Axis,    "X-axis"},
    {Type::Axis,    "Y-axis"},
    {Type::Button,  "Left"  },
    {Type::Button,  "Middle"},
    {Type::Button,  "Right" },
    {Type::Control, "Start" }
  };

  if(device == ID::Device::SegaTap) {
    vector<Input> inputs;
    for(char p : {'A','B','C','D'}) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"   }},
      {Type::Hat,     {"Port ", p, " - ", "Down" }},
      {Type::Hat,     {"Port ", p, " - ", "Left" }},
      {Type::Hat,     {"Port ", p, " - ", "Right"}},
      {Type::Button,  {"Port ", p, " - ", "A"    }},
      {Type::Button,  {"Port ", p, " - ", "B"    }},
      {Type::Button,  {"Port ", p, " - ", "C"    }},
      {Type::Button,  {"Port ", p, " - ", "X"    }},
      {Type::Button,  {"Port ", p, " - ", "Y"    }},
      {Type::Button,  {"Port ", p, " - ", "Z"    }},
      {Type::Control, {"Port ", p, " - ", "Mode" }},
      {Type::Control, {"Port ", p, " - ", "Start"}}
    });
    return inputs;
  }

  if(device == ID::Device::EA4WayPlay) {
    vector<Input> inputs;
    for(uint p = 1; p <= 4; p++) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"   }},
      {Type::Hat,     {"Port ", p, " - ", "Down" }},
      {Type::Hat,     {"Port ", p, " - ", "Left" }},
      {Type::Hat,     {"Port ", p, " - ", "Right"}},
      {Type::Button,  {"Port ", p, " - ", "A"    }},
      {Type::Button,  {"Port ", p, " - ", "B"    }},
      {Type::Button,  {"Port ", p, " - ", "C"    }},
    //{Type::Button,  {"Port ", p, " - ", "X"    }},
    //{Type::Button,  {"Port ", p, " - ", "Y"    }},
    //{Type::Button,  {"Port ", p, " - ", "Z"    }},
    //{Type::Control, {"Port ", p, " - ", "Mode" }},
      {Type::Control, {"Port ", p, " - ", "Start"}}
    });
    return inputs;
  }

  if(device == ID::Device::EA4WayPlay_) return {
  };

  return {};
}

auto MegaDriveInterface::connected(uint port) -> uint {
  if(port == ID::Port::Controller1) return option.port.controller1.device();
  if(port == ID::Port::Controller2) return option.port.controller2.device();
  if(port == ID::Port::Extension) return option.port.extension.device();
  return 0;
}

auto MegaDriveInterface::connect(uint port, uint device) -> void {
  if(port == ID::Port::Controller1) controllerPort1.connect(option.port.controller1.device(device));
  if(port == ID::Port::Controller2) controllerPort2.connect(option.port.controller2.device(device));
  if(port == ID::Port::Extension) extensionPort.connect(option.port.extension.device(device));
}

auto MegaDriveInterface::power() -> void {
  system.power(/* reset = */ false);
}

auto MegaDriveInterface::reset() -> void {
  system.power(/* reset = */ true);
}

auto MegaDriveInterface::run() -> void {
  system.run();
}

auto MegaDriveInterface::serialize() -> serializer {
  system.runToSave();
  return system.serialize();
}

auto MegaDriveInterface::unserialize(serializer& s) -> bool {
  return system.unserialize(s);
}

auto MegaDriveInterface::cheats(const vector<string>& list) -> void {
  cheat.assign(list);
}

auto MegaDriveInterface::options() -> Settings& {
  return option;
}

auto MegaDriveInterface::properties() -> Settings& {
  return property;
}

}
