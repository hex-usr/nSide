AboutWindow::AboutWindow() {
  aboutWindow = this;

  setTitle({"About ", nSide::Name, " ..."});
  setBackgroundColor({255, 255, 240});
  layout.setPadding(10);
  auto logo = image{Resource::Logo};
  logo.alphaBlend(0xfffff0);
  canvas.setIcon(logo);
  informationLeft.setFont(Font().setBold()).setAlignment(1.0).setText({
    "Version:\n",
    "Based on:\n",
    "Fork Author:\n",
    "License:\n",
    "Website of ", higan::Name, ":"
  });
  informationRight.setFont(Font().setBold()).setAlignment(0.0).setText({
    nSide::Version, "\n",
    "higan ", higan::Version, "\n",
    nSide::Author, "\n",
    higan::License, "\n",
    higan::Website
  });
  informationBottom.setFont(Font().setBold()).setAlignment(0.0).setText({
    "Note: ", nSide::Name, " does not emulate the Super Famicom (SNES).\n"
    "Please use bsnes or byuu if you wish to emulate the Super Famicom.\n"
    "\n"
    "Contributors to ", higan::Name, " (and ", nSide::Name, " by proxy):\n",
    nSide::Contributors.merge("\n")
  });

  setResizable(false);
  setSize(layout.minimumSize());
  setCentered();
  setDismissable();
}
