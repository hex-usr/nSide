#include "../nSide.hpp"
#include "about.cpp"
unique_pointer<AboutWindow> aboutWindow;
unique_pointer<Presentation> presentation;

Presentation::Presentation() {
  presentation = this;

  refreshLocale();
  updateRecentList();

  systemMenu.setVisible(false);

  updateSizeMenu();
  centerViewport.onActivate([&] {
    settings["View/Output"].setValue("Center");
    resizeViewport();
  });
  scaleViewport.onActivate([&] {
    settings["View/Output"].setValue("Scale");
    resizeViewport();
  });
  stretchViewport.onActivate([&] {
    settings["View/Output"].setValue("Stretch");
    resizeViewport();
  });
  if(settings["View/Output"].text() == "Center") centerViewport.setChecked();
  if(settings["View/Output"].text() == "Scale") scaleViewport.setChecked();
  if(settings["View/Output"].text() == "Stretch") stretchViewport.setChecked();
  adaptiveSizing.setChecked(settings["View/Adaptive"].boolean()).onToggle([&] {
    settings["View/Adaptive"].setValue(adaptiveSizing.checked());
    resizeWindow();
  });
  aspectCorrection.setChecked(settings["View/AspectCorrection"].boolean()).onToggle([&] {
    settings["View/AspectCorrection"].setValue(aspectCorrection.checked());
    resizeWindow();
  });
  showOverscanArea.setChecked(settings["View/Overscan"].boolean()).onToggle([&] {
    settings["View/Overscan"].setValue(showOverscanArea.checked());
    resizeWindow();
  });
  blurEmulation.setChecked(settings["Video/BlurEmulation"].boolean()).onToggle([&] {
    settings["Video/BlurEmulation"].setValue(blurEmulation.checked());
    if(emulator) {
      emulator->setOption("video/colorBleed", blurEmulation.checked());
      emulator->setOption("video/interframeBlending", blurEmulation.checked());
    }
  });
  colorEmulation.setChecked(settings["Video/ColorEmulation"].boolean()).onToggle([&] {
    settings["Video/ColorEmulation"].setValue(colorEmulation.checked());
    if(emulator) emulator->setOption("video/colorEmulation", colorEmulation.checked());
  });
  scanlineEmulation.setChecked(settings["Video/ScanlineEmulation"].boolean()).setVisible(false).onToggle([&] {
    settings["Video/ScanlineEmulation"].setValue(scanlineEmulation.checked());
    if(emulator) emulator->setOption("video/scanlineEmulation", scanlineEmulation.checked());
  });
  videoShaderNone.onActivate([&] {
    settings["Video/Shader"].setValue("None");
    program->updateVideoShader();
  });
  videoShaderBlur.onActivate([&] {
    settings["Video/Shader"].setValue("Blur");
    program->updateVideoShader();
  });
  loadShaders();
  synchronizeVideo.setChecked(settings["Video/Synchronize"].boolean()).setVisible(false).onToggle([&] {
    settings["Video/Synchronize"].setValue(synchronizeVideo.checked());
    video->setBlocking(synchronizeVideo.checked());
  });
  synchronizeAudio.setChecked(settings["Audio/Synchronize"].boolean()).onToggle([&] {
    settings["Audio/Synchronize"].setValue(synchronizeAudio.checked());
    audio->setBlocking(synchronizeAudio.checked());
  });
  muteAudio.setChecked(settings["Audio/Mute"].boolean()).onToggle([&] {
    settings["Audio/Mute"].setValue(muteAudio.checked());
    program->updateAudioEffects();
  });
  showStatusBar.setChecked(settings["View/StatusBar"].boolean()).onToggle([&] {
    settings["View/StatusBar"].setValue(showStatusBar.checked());
    if(!showStatusBar.checked()) {
      layout.remove(statusLayout);
    } else {
      layout.append(statusLayout, Size{~0, StatusHeight});
    }
    if(visible()) resizeWindow();
  });
  showSystemSettings.setIcon(Icon::Device::Storage).onActivate([&] { settingsManager->show(0); });
  showVideoSettings.setIcon(Icon::Device::Display).onActivate([&] { settingsManager->show(1); });
  showAudioSettings.setIcon(Icon::Device::Speaker).onActivate([&] { settingsManager->show(2); });
  showInputSettings.setIcon(Icon::Device::Joypad).onActivate([&] {
    if(emulator) {
      //default input panel to current core's input settings
      for(auto item : settingsManager->input.emulatorList.items()) {
        if(systemMenu.text() == item.text()) {
          item.setSelected();
          settingsManager->input.emulatorList.doChange();
          break;
        }
      }
    }
    settingsManager->show(3);
  });
  showHotkeySettings.setIcon(Icon::Device::Keyboard).onActivate([&] { settingsManager->show(4); });
  showAdvancedSettings.setIcon(Icon::Action::Settings).onActivate([&] { settingsManager->show(5); });

  toolsMenu.setVisible(false);
  saveSlot0.onActivate([&] { program->saveState(0); });
  saveSlot1.onActivate([&] { program->saveState(1); });
  saveSlot2.onActivate([&] { program->saveState(2); });
  saveSlot3.onActivate([&] { program->saveState(3); });
  saveSlot4.onActivate([&] { program->saveState(4); });
  saveSlot5.onActivate([&] { program->saveState(5); });
  saveSlot6.onActivate([&] { program->saveState(6); });
  saveSlot7.onActivate([&] { program->saveState(7); });
  saveSlot8.onActivate([&] { program->saveState(8); });
  saveSlot9.onActivate([&] { program->saveState(9); });
  loadSlot0.onActivate([&] { program->loadState(0); });
  loadSlot1.onActivate([&] { program->loadState(1); });
  loadSlot2.onActivate([&] { program->loadState(2); });
  loadSlot3.onActivate([&] { program->loadState(3); });
  loadSlot4.onActivate([&] { program->loadState(4); });
  loadSlot5.onActivate([&] { program->loadState(5); });
  loadSlot6.onActivate([&] { program->loadState(6); });
  loadSlot7.onActivate([&] { program->loadState(7); });
  loadSlot8.onActivate([&] { program->loadState(8); });
  loadSlot9.onActivate([&] { program->loadState(9); });
  pauseEmulation.onToggle([&] { program->togglePause(); });
  cheatEditor.setIcon(Icon::Edit::Replace).onActivate([&] { toolsManager->show(0); });
  stateManager.setIcon(Icon::Application::FileManager).onActivate([&] { toolsManager->show(1); });
  manifestViewer.setIcon(Icon::Emblem::Text).onActivate([&] { toolsManager->show(2); });
  gameNotes.setIcon(Icon::Emblem::Text).onActivate([&] { toolsManager->show(3); });

  documentation.setIcon(Icon::Application::Browser).onActivate([&] {
    invoke("https://doc.byuu.org/higan/");
  });
  //Wait for this web page to go online before adding this menu item
//credits.setIcon(Icon::Application::Browser).setText("Credits ...").onActivate([&] {
//  invoke("https://doc.byuu.org/higan/credits/");
//});
  about.setIcon(Icon::Prompt::Question).onActivate([&] {
    aboutWindow->setVisible().setFocused();
  });

  viewport.setDroppable().onDrop([&](vector<string> locations) {
    if(!locations || !directory::exists(locations.first())) return;
    program->gameQueue.append(locations.first());
    program->load();
  }).onSize([&] {
    configureViewport();
  });

  iconLayout.setAlignment(0.0);
  image icon{Resource::Icon};
  icon.alphaBlend(0x000000);
  iconCanvas.setIcon(icon);

  if(!settings["View/StatusBar"].boolean()) {
    layout.remove(statusLayout);
  }

  auto font = Font().setBold();
  auto back = Color{ 32,  32,  32};
  auto fore = Color{255, 255, 255};

  spacerLeft.setBackgroundColor(back);

  statusMessage.setFont(font);
  statusMessage.setAlignment(0.0);
  statusMessage.setBackgroundColor(back);
  statusMessage.setForegroundColor(fore);

  statusInfo.setFont(font);
  statusInfo.setAlignment(1.0);
  statusInfo.setBackgroundColor(back);
  statusInfo.setForegroundColor(fore);
  statusInfo.setText("Unloaded");

  spacerRight.setBackgroundColor(back);

  onSize([&] {
    resizeViewport();
  });

  onClose([&] {
    program->quit();
  });

  settings["View/Multiplier"].setValue(2);

  setTitle({"nSide v", nSide::Version});
  setBackgroundColor({0, 0, 0});
  resizeWindow();
  setCentered();

  #if defined(PLATFORM_MACOS)
  about.setVisible(false);
  Application::Cocoa::onAbout([&] { about.doActivate(); });
  Application::Cocoa::onActivate([&] { setFocused(); });
  Application::Cocoa::onPreferences([&] { showInputSettings.doActivate(); });
  Application::Cocoa::onQuit([&] { doClose(); });
  #endif
}

auto Presentation::refreshLocale() -> void {
  recentMenu.setText(locale["Menu/Recent"]);
  systemsMenu.setText(locale["Menu/Systems"]);

  settingsMenu.setText(locale["Menu/Settings"]);
  sizeMenu.setText(locale["Menu/Settings/Size"]);
  outputMenu.setText(locale["Menu/Settings/Output"]);
  centerViewport.setText(locale["Menu/Settings/Output/Center"]);
  scaleViewport.setText(locale["Menu/Settings/Output/Scale"]);
  stretchViewport.setText(locale["Menu/Settings/Output/Stretch"]);
  adaptiveSizing.setText(locale["Menu/Settings/AdaptiveSizing"]);
  aspectCorrection.setText(locale["Menu/Settings/AspectCorrection"]);
  showOverscanArea.setText(locale["Menu/Settings/ShowOverscanArea"]);
  videoEmulationMenu.setText(locale["Menu/Settings/VideoEmulation"]);
  blurEmulation.setText(locale["Menu/Settings/VideoEmulation/Blurring"]);
  colorEmulation.setText(locale["Menu/Settings/VideoEmulation/Colors"]);
  scanlineEmulation.setText(locale["Menu/Settings/VideoEmulation/Scanlines"]);
  videoShaderMenu.setText(locale["Menu/Settings/VideoShader"]);
  videoShaderNone.setText(locale["Menu/Settings/VideoShader/None"]);
  videoShaderBlur.setText(locale["Menu/Settings/VideoShader/Blur"]);
  synchronizeVideo.setText(locale["Menu/Settings/SynchronizeVideo"]);
  synchronizeAudio.setText(locale["Menu/Settings/SynchronizeAudio"]);
  muteAudio.setText(locale["Menu/Settings/MuteAudio"]);
  showStatusBar.setText(locale["Menu/Settings/ShowStatusBar"]);
  showSystemSettings.setText(locale["Menu/Settings/Systems..."]);
  showVideoSettings.setText(locale["Menu/Settings/Video..."]);
  showAudioSettings.setText(locale["Menu/Settings/Audio..."]);
  showInputSettings.setText(locale["Menu/Settings/Input..."]);
  showHotkeySettings.setText(locale["Menu/Settings/Hotkeys..."]);
  showAdvancedSettings.setText(locale["Menu/Settings/Advanced..."]);

  toolsMenu.setText(locale["Menu/Tools"]);
  saveQuickStateMenu.setText(locale["Menu/Tools/SaveQuickState"]);
  saveSlot0.setText(locale["Menu/Tools/Slot"].replace("%d", "0"));
  saveSlot1.setText(locale["Menu/Tools/Slot"].replace("%d", "1"));
  saveSlot2.setText(locale["Menu/Tools/Slot"].replace("%d", "2"));
  saveSlot3.setText(locale["Menu/Tools/Slot"].replace("%d", "3"));
  saveSlot4.setText(locale["Menu/Tools/Slot"].replace("%d", "4"));
  saveSlot5.setText(locale["Menu/Tools/Slot"].replace("%d", "5"));
  saveSlot6.setText(locale["Menu/Tools/Slot"].replace("%d", "6"));
  saveSlot7.setText(locale["Menu/Tools/Slot"].replace("%d", "7"));
  saveSlot8.setText(locale["Menu/Tools/Slot"].replace("%d", "8"));
  saveSlot9.setText(locale["Menu/Tools/Slot"].replace("%d", "9"));
  loadQuickStateMenu.setText(locale["Menu/Tools/LoadQuickState"]);
  loadSlot0.setText(locale["Menu/Tools/Slot"].replace("%d", "0"));
  loadSlot1.setText(locale["Menu/Tools/Slot"].replace("%d", "1"));
  loadSlot2.setText(locale["Menu/Tools/Slot"].replace("%d", "2"));
  loadSlot3.setText(locale["Menu/Tools/Slot"].replace("%d", "3"));
  loadSlot4.setText(locale["Menu/Tools/Slot"].replace("%d", "4"));
  loadSlot5.setText(locale["Menu/Tools/Slot"].replace("%d", "5"));
  loadSlot6.setText(locale["Menu/Tools/Slot"].replace("%d", "6"));
  loadSlot7.setText(locale["Menu/Tools/Slot"].replace("%d", "7"));
  loadSlot8.setText(locale["Menu/Tools/Slot"].replace("%d", "8"));
  loadSlot9.setText(locale["Menu/Tools/Slot"].replace("%d", "9"));
  pauseEmulation.setText(locale["Menu/Tools/PauseEmulation"]);
  cheatEditor.setText(locale["Menu/Tools/CheatEditor..."]);
  stateManager.setText(locale["Menu/Tools/StateManager..."]);
  manifestViewer.setText(locale["Menu/Tools/ManifestViewer..."]);
  gameNotes.setText(locale["Menu/Tools/GameNotes..."]);

  helpMenu.setText(locale["Menu/Help"]);
  documentation.setText(locale["Menu/Help/Documentation..."]);
  about.setText(locale["Menu/Help/About..."]);
}

auto Presentation::updateRecentList() -> void {
  if(settings["Library/RecentList"].boolean() && emulator) {
    uint position = RecentGames;
    for(uint n : range(RecentGames)) {
      auto paths = settings[{"Recent/", n, "/Path"}].text().trim("\"", "\"", 1).split("\" \"");
      if(paths.size() != program->gamePaths.size() - 1) continue;
      bool match = true;
      for(uint n : range(paths.size())) if(paths[n] != program->path(n + 1)) {
        match = false;
        break;
      }
      if(match) {
        position = n;
        break;
      }
    }
    if(position != 0) {
      for(uint n : reverse(range(min(position, RecentGames - 1)))) {
        settings[{"Recent/", n + 1, "/Title"}].setValue(settings[{"Recent/", n, "/Title"}].text());
        settings[{"Recent/", n + 1, "/Path" }].setValue(settings[{"Recent/", n, "/Path" }].text());
      }
      settings["Recent/0/Title"].setValue(emulator->titles().merge(" + "));
      vector<string> paths;
      for(uint n : range(program->gamePaths.size() - 1)) {
        paths.append(program->path(n + 1));
      }
      settings["Recent/0/Path"].setValue({"\"", paths.merge("\" \""),"\""});
    }
  }

  if(settings["Library/RecentList"].boolean()) {
    recentMenu.setVisible(!!settings["Recent/0/Path"].value()).reset();
    for(const auto& entry : settings["Recent"]) {
      if(!entry["Path"].text()) continue;
      auto item = new MenuItem{&recentMenu};
      item->setText(entry["Title"].text()).onActivate([=] {
        program->unload();
        program->gameQueue = entry["Path"].text().trim("\"", "\"", 1).split("\" \"");
        program->load();
      });
    }
  } else {
    recentMenu.setVisible(false);
  }
}

auto Presentation::updateEmulatorMenu() -> void {
  if(!emulator) return;
  auto information = emulator->information();

  systemMenu.reset();
  for(auto& port : emulator->ports()) {
    Menu menu{&systemMenu};
    menu.setProperty("portID", port.id);
    menu.setText(port.name);
    if(port.name.beginsWith("Expansion") || port.name.beginsWith("Extension")) {
      menu.setIcon(Icon::Device::Storage);
    } else {
      menu.setIcon(Icon::Device::Joypad);
    }

    auto path = string{information.name, "/", port.name}.replace(" ", "");
    auto deviceName = settings(path).text();
    auto deviceID = emulator->connected(port.id);

    Group devices;
    for(auto& device : emulator->devices(port.id)) {
      MenuRadioItem item{&menu};
      item.setProperty("deviceID", device.id);
      item.setText(device.name);
      item.onActivate([=] {
        settings(path).setValue(device.name);
        emulator->connect(port.id, device.id);
        updateEmulatorDeviceSelections();
      });
      devices.append(item);

      if(deviceName == device.name) item.doActivate();
      if(!deviceName && deviceID == device.id) item.doActivate();
    }

    if(devices.objectCount() == 0) {
      menu.setVisible(false);
    }
  }

  if(systemMenu.actionCount()) {
    systemMenu.append(MenuSeparator());
  }

  if(information.resettable) {
    systemMenu.append(MenuItem().setText(locale["Menu/System/SoftReset"]).setIcon(Icon::Action::Refresh).onActivate([&] {
      program->softReset();
    }));
  }

  systemMenu.append(MenuItem().setText(locale["Menu/System/PowerCycle"]).setIcon(Icon::Action::Refresh).onActivate([&] {
    program->powerCycle();
  }));

  systemMenu.append(MenuItem().setText(locale["Menu/System/Unload"]).setIcon(Icon::Media::Eject).onActivate([&] {
    program->unload();
  }));

  updateEmulatorDeviceSelections();
}

auto Presentation::updateEmulatorDeviceSelections() -> void {
  if(!emulator) return;

  for(auto& port : emulator->ports()) {
    for(auto& action : systemMenu->actions()) {
      auto portID = action.property("portID");
      if(portID && portID.natural() == port.id) {
        if(auto menu = action.cast<Menu>()) {
          auto deviceID = emulator->connected(port.id);
          for(auto& action : menu.actions()) {
            if(auto item = action.cast<MenuRadioItem>()) {
              if(item.property("deviceID").natural() == deviceID) {
                item.setChecked();
              }
            }
          }
        }
      }
    }
  }
}

auto Presentation::updateSizeMenu() -> void {
  assert(sizeMenu.actionCount() == 0);  //should only be called once

  //determine the largest multiplier that can be used by the largest monitor found
  uint height = 1;
  for(uint monitor : range(Monitor::count())) {
    height = max(height, Monitor::workspace(monitor).height());
  }

  uint multipliers = max(1, height / 240);
  for(uint multiplier : range(1, multipliers + 1)) {
    MenuRadioItem item{&sizeMenu};
    item.setProperty("multiplier", multiplier);
    item.setText({multiplier, "× (", 240 * multiplier, "p)"});
    item.onActivate([=] {
      settings["View/Multiplier"].setValue(multiplier);
      resizeWindow();
    });
    sizeGroup.append(item);
  }

  for(auto item : sizeGroup.objects<MenuRadioItem>()) {
    if(settings["View/Multiplier"].natural() == item.property("multiplier").natural()) {
      item.setChecked();
    }
  }

  sizeMenu.append(MenuSeparator());
  sizeMenu.append(MenuItem().setIcon(Icon::Action::Remove).setText("Shrink Window To Size").onActivate([&] {
    resizeWindow();
  }));
  sizeMenu.append(MenuItem().setIcon(Icon::Place::Settings).setText("Center Window").onActivate([&] {
    setCentered();
  }));
}

auto Presentation::configureViewport() -> void {
  uint width = viewport.geometry().width();
  uint height = viewport.geometry().height();
  if(video) video->configure(width, height, 60, 60);
}

auto Presentation::clearViewport() -> void {
  if(!emulator || !emulator->loaded()) viewportLayout.setPadding();
  if(!visible() || !video) return;

  uint width = 16;
  uint height = 16;
  if(auto [output, length] = video->acquire(width, height); output) {
    for(uint y : range(height)) {
      auto line = output + y * (length >> 2);
      for(uint x : range(width)) *line++ = 0xff000000;
    }

    video->release();
    video->output();
  }
}

auto Presentation::resizeViewport() -> void {
  uint layoutWidth = viewportLayout.geometry().width();
  uint layoutHeight = viewportLayout.geometry().height();

  uint width = 320;
  uint height = 240;

  if(emulator) {
    auto display = emulator->display();
    width = display.width;
    height = display.height;
    if(settings["View/AspectCorrection"].boolean()) width *= display.aspectCorrection;
    if(!settings["View/Overscan"].boolean()) {
      if(display.type == higan::Interface::Display::Type::CRT) {
        uint overscanHorizontal = settings["View/Overscan/Horizontal"].natural();
        uint overscanVertical = settings["View/Overscan/Vertical"].natural();
        width -= overscanHorizontal * 2;
        height -= overscanVertical * 2;
      }
    }
  }

  if(visible() && !fullScreen()) {
    uint widthMultiplier = layoutWidth / width;
    uint heightMultiplier = layoutHeight / height;
    uint multiplier = max(1, min(widthMultiplier, heightMultiplier));
    settings["View/Multiplier"].setValue(multiplier);
    for(auto item : sizeGroup.objects<MenuRadioItem>()) {
      if(auto property = item.property("multiplier")) {
        if(property.natural() == multiplier) item.setChecked();
      }
    }
  }

  if(!emulator || !emulator->loaded()) return clearViewport();
  if(!video) return;

  uint viewportWidth;
  uint viewportHeight;
  if(settings["View/Output"].text() == "Center") {
    uint widthMultiplier = layoutWidth / width;
    uint heightMultiplier = layoutHeight / height;
    uint multiplier = min(widthMultiplier, heightMultiplier);
    viewportWidth = width * multiplier;
    viewportHeight = height * multiplier;
  } else if(settings["View/Output"].text() == "Scale") {
    double widthMultiplier = (double)layoutWidth / width;
    double heightMultiplier = (double)layoutHeight / height;
    double multiplier = min(widthMultiplier, heightMultiplier);
    viewportWidth = width * multiplier;
    viewportHeight = height * multiplier;
  } else if(settings["View/Output"].text() == "Stretch" || 1) {
    viewportWidth = layoutWidth;
    viewportHeight = layoutHeight;
  }

  //center viewport within viewportLayout by use of viewportLayout padding
  uint paddingWidth = layoutWidth - viewportWidth;
  uint paddingHeight = layoutHeight - viewportHeight;
  viewportLayout.setPadding({
    paddingWidth / 2, paddingHeight / 2,
    paddingWidth - paddingWidth / 2, paddingHeight - paddingHeight / 2
  });
}

auto Presentation::resizeWindow() -> void {
  if(fullScreen()) return;
  if(maximized()) setMaximized(false);

  uint width = 320;
  uint height = 240;
  uint multiplier = max(1, settings["View/Multiplier"].natural());
  uint statusHeight = settings["View/StatusBar"].boolean() ? StatusHeight : 0;

  if(emulator) {
    auto display = emulator->display();
    width = display.width;
    height = display.height;
    if(settings["View/AspectCorrection"].boolean()) width *= display.aspectCorrection;
    if(!settings["View/Overscan"].boolean()) {
      if(display.type == higan::Interface::Display::Type::CRT) {
        uint overscanHorizontal = settings["View/Overscan/Horizontal"].natural();
        uint overscanVertical = settings["View/Overscan/Vertical"].natural();
        width -= overscanHorizontal * 2;
        height -= overscanVertical * 2;
      }
    }
  }

  setMinimumSize({width, height + statusHeight});
  setSize({width * multiplier, height * multiplier + statusHeight});
  layout.setGeometry(layout.geometry());
  resizeViewport();
}

auto Presentation::toggleFullScreen() -> void {
  if(!fullScreen()) {
    if(settings["View/StatusBar"].boolean()) {
      layout.remove(statusLayout);
    }
    menuBar.setVisible(false);
    setFullScreen(true);
    video->setExclusive(settings["Video/Exclusive"].boolean());
    if(video->exclusive()) setVisible(false);
    if(!input->acquired()) input->acquire();
    resizeViewport();
  } else {
    if(input->acquired()) input->release();
    if(video->exclusive()) setVisible(true);
    video->setExclusive(false);
    setFullScreen(false);
    menuBar.setVisible(true);
    if(settings["View/StatusBar"].boolean()) {
      layout.append(statusLayout, Size{~0, StatusHeight});
    }
    resizeWindow();
    setCentered();
  }
}

auto Presentation::loadSystems() -> void {
  systemsMenu.reset();
  for(auto system : settings.find("Systems/System")) {
    if(!system["Visible"].boolean()) continue;
    MenuItem item{&systemsMenu};
    string name = system.text();
    string filename = system["Load"].text();
    string load = Location::base(filename).trimRight("/", 1L);
    string alias = system["Alias"].text();
    item.setIcon(load ? (image)Icon::Emblem::Folder : (image)Icon::Device::Storage);
    item.setText({alias ? alias : load ? load : name, " ..."});
    item.onActivate([=] {
      for(auto& emulator : program->emulators) {
        auto information = emulator->information();
        if(name == information.name) {
          if(filename) program->gameQueue.append(filename);
          program->load(*emulator);
          break;
        }
      }
    });
  }

  //add cart-pal menu option -- but only if cart-pal binary is present
  if(execute("cart-pal", "--name").output.strip() == "cart-pal") {
    if(systemsMenu.actionCount()) systemsMenu.append(MenuSeparator());
    MenuItem item{&systemsMenu};
    item.setIcon(Icon::Emblem::File);
    item.setText(locale["Menu/Systems/LoadROMFile..."]);
    item.onActivate([&] {
      audio->clear();
      if(auto location = execute("cart-pal", "--import")) {
        program->gameQueue.append(location.output.strip());
        program->load();
      }
    });
  }
}

auto Presentation::loadShaders() -> void {
  auto pathname = locate("shaders/");

  if(settings["Video/Driver"].text() == "OpenGL 3.2") {
    for(auto shader : directory::folders(pathname, "*.shader")) {
      if(videoShaders.objectCount() == 2/*3*/) videoShaderMenu.append(MenuSeparator());
      MenuRadioItem item{&videoShaderMenu};
      item.setText(string{shader}.trimRight(".shader/", 1L)).onActivate([=] {
        settings["Video/Shader"].setValue({pathname, shader});
        program->updateVideoShader();
      });
      videoShaders.append(item);
    }
  }

  if(settings["Video/Shader"].text() == "None") videoShaderNone.setChecked();
  if(settings["Video/Shader"].text() == "Blur") videoShaderBlur.setChecked();
  for(auto radioItem : videoShaders.objects<MenuRadioItem>()) {
    if(settings["Video/Shader"].text() == string{pathname, radioItem.text(), ".shader/"}) {
      radioItem.setChecked();
    }
  }
}
