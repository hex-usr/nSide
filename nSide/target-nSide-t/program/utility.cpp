auto Program::initializeVideoDriver() -> void {
  if(!Video::hasDrivers().find(settings["Video/Driver"].text())) {
    settings["Video/Driver"].setValue(Video::safestDriver());
  }
  video = new Video;
  video->create(settings["Video/Driver"].text());
  video->setContext(presentation->viewport.handle());

  video->setBlocking(settings["Video/Synchronize"].boolean());

  if(!video->ready()) {
    MessageDialog().setText(locale["Error/VideoDriver"]).warning();
    video = new Video;
    video->create("None");
  }

  video->onUpdate([&](uint width, uint height) {
    if(!emulator || !emulator->loaded()) presentation->clearViewport();
  });

  presentation->configureViewport();
  presentation->clearViewport();
}

auto Program::initializeAudioDriver() -> void {
  if(!Audio::hasDrivers().find(settings["Audio/Driver"].text())) {
    settings["Audio/Driver"].setValue("None");
  }
  audio = new Audio;
  audio->create(settings["Audio/Driver"].text());
  audio->setContext(presentation->viewport.handle());

  if(!audio->hasDevices().find(settings["Audio/Device"].text())) {
    settings["Audio/Device"].setValue(audio->device());
  }
  audio->setDevice(settings["Audio/Device"].text());

  if(!audio->hasFrequencies().find(settings["Audio/Frequency"].natural())) {
    settings["Audio/Frequency"].setValue(audio->frequency());
  }
  audio->setFrequency(settings["Audio/Frequency"].natural());

  if(!audio->hasLatencies().find(settings["Audio/Latency"].natural())) {
    settings["Audio/Latency"].setValue(audio->latency());
  }
  audio->setLatency(settings["Audio/Latency"].natural());

  audio->setChannels(2);
  audio->setExclusive(settings["Audio/Exclusive"].boolean());
  audio->setBlocking(settings["Audio/Synchronize"].boolean());

  if(!audio->ready()) {
    MessageDialog().setText(locale["Error/AudioDriver"]).warning();
    audio = new Audio;
    audio->create("None");
  }

  higan::audio.setFrequency(settings["Audio/Frequency"].real());
}

auto Program::initializeInputDriver() -> void {
  if(!Input::hasDrivers().find(settings["Input/Driver"].text())) {
    settings["Input/Driver"].setValue("None");
  }
  input = new Input;
  input->create(settings["Input/Driver"].text());
  input->setContext(presentation->viewport.handle());

  if(!input->ready()) {
    MessageDialog().setText(locale["Error/InputDriver"]).warning();
    input = new Input;
    input->create("None");
  }
}

auto Program::softReset() -> void {
  if(!emulator || !emulator->information().resettable) return;
  emulator->reset();
  showMessage(locale["Status/SoftReset"]);
}

auto Program::powerCycle() -> void {
  if(!emulator) return;
  emulator->power();
  showMessage(locale["Status/PowerCycle"]);
}

auto Program::togglePause() -> void {
  program->pause = !program->pause;
  presentation->pauseEmulation.setChecked(program->pause);
  if(program->pause) {
    showMessage(locale["Status/Pausing"]);
  } else {
    showMessage(locale["Status/Unpausing"]);
  }
}

auto Program::rotateDisplay() -> void {
  if(!emulator) return;
  if(!emulator->hasOption("video/rotateDisplay")) return showMessage(locale["Status/RotateUnsupported"]);
  auto rotate = emulator->getOption("video/rotateDisplay");
  emulator->setOption("video/rotateDisplay", !(rotate == "true"));
  presentation->resizeViewport();
  showMessage(locale["Status/DisplayRotated"]);
}

auto Program::showMessage(const string& text) -> void {
  statusTime = time(nullptr);
  statusMessage = text;
}

auto Program::updateStatusText() -> void {
  string message;
  if(chrono::timestamp() - statusTime <= 2) {
    message = statusMessage;
  }
  if(message != presentation->statusMessage.text()) {
    presentation->statusMessage.setText(message);
  }

  string info;
  if(!emulator || !emulator->loaded()) {
    info = locale["Status/Unloaded"];
  } else if(pause || (!focused() && settingsManager->input.pauseEmulation.checked())) {
    info = locale["Status/Paused"];
  } else {
    info = statusInfo;
  }

  if(info != presentation->statusInfo.text()) {
    presentation->statusInfo.setText(info);
  }
}

auto Program::updateVideoPalette() -> void {
  double saturation = settings["Video/Saturation"].natural() / 100.0;
  double gamma = settings["Video/Gamma"].natural() / 100.0;
  double luminance = settings["Video/Luminance"].natural() / 100.0;
  higan::video.setSaturation(saturation);
  higan::video.setGamma(gamma);
  higan::video.setLuminance(luminance);
  higan::video.setPalette();
}

auto Program::updateVideoShader() -> void {
  video->setShader(settings["Video/Shader"].text());
}

auto Program::updateAudioDriver() -> void {
  if(!audio) return;
  audio->clear();
  audio->setDevice(settings["Audio/Device"].text());
  audio->setExclusive(settings["Audio/Exclusive"].boolean());
  audio->setFrequency(settings["Audio/Frequency"].real());
  audio->setLatency(settings["Audio/Latency"].natural());
  higan::audio.setFrequency(settings["Audio/Frequency"].real());
}

auto Program::updateAudioEffects() -> void {
  auto volume = settings["Audio/Mute"].boolean() ? 0.0 : settings["Audio/Volume"].natural() * 0.01;
  higan::audio.setVolume(volume);

  auto balance = max(-1.0, min(1.0, (settings["Audio/Balance"].integer() - 50) / 50.0));
  higan::audio.setBalance(balance);
}

auto Program::focused() -> bool {
  //exclusive mode creates its own top-level window: presentation window will not have focus
  if(video->exclusive()) return true;
  if(presentation && presentation->focused()) return true;
  return false;
}

auto Program::isNS() -> bool {
  static vector<string> higanSystems = {
    //"Famicom",
    "Super Famicom",
    //"Master System",  //CPU serializes cartridge/MyCard/expansion disable flags
    "Mega Drive",
    "PC Engine",
    "SuperGrafx",
    "Game Boy",
    "Game Boy Color",
    //"Game Boy Advance",  //Fast PPU option
    "Game Gear",
    "WonderSwan",
    "WonderSwan Color",
    "Pocket Challenge V2"
  };

  if(!emulator) return false;
  if(higanSystems.find(emulator->information().name)) return false;
  return true;
}
