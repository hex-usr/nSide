AdvancedSettings::AdvancedSettings(TabFrame* parent) : TabFrameItem(parent) {
  setIcon(Icon::Action::Settings);

  layout.setPadding(5);

  localeLabel.setFont(Font().setBold());
  localeSelect.onChange([&] {
    settings["Locale"].setValue(localeSelect.selected().text());
    locale.load(settings["Locale"].text());
    locale.refresh();
  });
  ComboButtonItem localeAuto;
  localeAuto.setText("Auto");
  if(settings["Locale"].text() == "Auto") localeAuto.setSelected();
  localeSelect.append(localeAuto);
  for(auto& filename : directory::files(locate("locales/"), "*.bml")) {
    if(filename == "default.bml") continue;
    string locale = string{filename}.trimRight(".bml", 1L);
    ComboButtonItem item;
    item.setText(locale);
    localeSelect.append(item);
    if(settings["Locale"].text() == locale) item.setSelected();
  }

  driverLabel.setFont(Font().setBold());
  videoDriver.onChange([&] { settings["Video/Driver"].setValue(videoDriver.selected().text()); });
  for(auto& driver : Video::hasDrivers()) {
    ComboButtonItem item{&videoDriver};
    item.setText(driver);
    if(settings["Video/Driver"].text() == driver) item.setSelected();
  }
  audioDriver.onChange([&] { settings["Audio/Driver"].setValue(audioDriver.selected().text()); });
  for(auto& driver : Audio::hasDrivers()) {
    ComboButtonItem item{&audioDriver};
    item.setText(driver);
    if(settings["Audio/Driver"].text() == driver) item.setSelected();
  }
  inputDriver.onChange([&] { settings["Input/Driver"].setValue(inputDriver.selected().text()); });
  for(auto& driver : Input::hasDrivers()) {
    ComboButtonItem item{&inputDriver};
    item.setText(driver);
    if(settings["Input/Driver"].text() == driver) item.setSelected();
  }

  libraryLabel.setFont(Font().setBold());
  libraryLocation.setEditable(false).setText(settings["Library/Location"].text());
  libraryChange.onActivate([&] {
    if(auto location = BrowserDialog().setTitle("Select Library Location").selectFolder()) {
      settings["Library/Location"].setValue(location);
      libraryLocation.setText(location);
    }
  });

  ignoreManifests.setChecked(settings["Library/IgnoreManifests"].boolean()).onToggle([&] {
    settings["Library/IgnoreManifests"].setValue(ignoreManifests.checked());
  });

  otherLabel.setText("Other").setFont(Font().setBold());
  autoSaveMemory.setChecked(settings["Emulation/AutoSaveMemory/Enable"].boolean()).onToggle([&] {
    settings["Emulation/AutoSaveMemory/Enable"].setValue(autoSaveMemory.checked());
  });
  recentList.setChecked(settings["Library/RecentList"].boolean()).onToggle([&] {
    if(!recentList.checked()) {
      string prompt = locale["Settings/Advanced/Other/RecentList/Clear"];
      string yes = locale["Settings/Advanced/Other/RecentList/Clear/Yes"];
      string no = locale["Settings/Advanced/Other/RecentList/Clear/No"];
      if(MessageDialog(prompt).setParent(*settingsManager).question({yes, no}) == yes) {
        for(uint index : range(10)) {
          settings[{"Recent/", index, "/Title"}].setValue("");
          settings[{"Recent/", index, "/Path"}].setValue("");
        }
      } else {
        recentList.setChecked(true);
      }
    }
    settings["Library/RecentList"].setValue(recentList.checked());
    presentation->updateRecentList();
  });
  fastProfile.setChecked(settings["Emulation/FastProfile"].boolean()).onToggle([&] {
    settings["Emulation/FastProfile"].setValue(fastProfile.checked());
  });
}

auto AdvancedSettings::refreshLocale() -> void {
  setText(locale["Settings/Advanced"]);

  localeLabel.setText("Locale:");

  driverLabel.setText(locale["Settings/Advanced/DriverSelection"]);
  videoLabel.setText(locale["Settings/Advanced/DriverSelection/Video"]);
  audioLabel.setText(locale["Settings/Advanced/DriverSelection/Audio"]);
  inputLabel.setText(locale["Settings/Advanced/DriverSelection/Input"]);

  libraryLabel.setText(locale["Settings/Advanced/GameLibrary"]);
  libraryPrefix.setText(locale["Settings/Advanced/GameLibrary/Location"]);
  libraryChange.setText(locale["Settings/Advanced/GameLibrary/Change..."]);
  ignoreManifests.setText(locale["Settings/Advanced/GameLibrary/IgnoreManifests"]);

  otherLabel.setText(locale["Settings/Advanced/Other"]);
  autoSaveMemory.setText(locale["Settings/Advanced/Other/AutoSaveMemory"]);
  recentList.setText(locale["Settings/Advanced/Other/RecentList"]);
  fastProfile.setText(locale["Settings/Advanced/Other/FastProfile"]);

  layout.setPadding(5);
}

auto AdvancedSettings::updateConfiguration() -> void {
  if(!emulator) return;
  emulator->setOption("hack/ppu/fast", fastProfile.checked());
  emulator->setOption("hack/dsp/fast", fastProfile.checked());
  emulator->setOption("hack/coprocessor/fast", fastProfile.checked());
}
