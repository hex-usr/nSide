VideoSettings::VideoSettings(TabFrame* parent) : TabFrameItem(parent) {
  setIcon(Icon::Device::Display);

  layout.setPadding(5);

  colorAdjustmentLabel.setFont(Font().setBold());
  saturationValue.setAlignment(0.5);
  saturationSlider.setLength(201).setPosition(settings["Video/Saturation"].natural()).onChange([&] { updateColor(); });
  gammaValue.setAlignment(0.5);
  gammaSlider.setLength(101).setPosition(settings["Video/Gamma"].natural() - 100).onChange([&] { updateColor(); });
  luminanceValue.setAlignment(0.5);
  luminanceSlider.setLength(101).setPosition(settings["Video/Luminance"].natural()).onChange([&] { updateColor(); });

  overscanMaskLabel.setFont(Font().setBold());
  horizontalMaskValue.setAlignment(0.5);
  horizontalMaskSlider.setLength(25).setPosition(settings["Video/Overscan/Horizontal"].natural()).onChange([&] { updateOverscan(); });
  verticalMaskValue.setAlignment(0.5);
  verticalMaskSlider.setLength(25).setPosition(settings["Video/Overscan/Vertical"].natural()).onChange([&] { updateOverscan(); });

  fullscreenLabel.setFont(Font().setBold());
  fullscreenExclusive.setChecked(settings["Video/Exclusive"].boolean()).onToggle([&] {
    settings["Video/Exclusive"].setValue(fullscreenExclusive.checked());
  });

  updateColor(true);
  updateOverscan(true);
}

auto VideoSettings::refreshLocale() -> void {
  setText(locale["Settings/Video"]);

  colorAdjustmentLabel.setText(locale["Settings/Video/ColorAdjustment"]);
  saturationLabel.setText(locale["Settings/Video/ColorAdjustment/Saturation"]);
  gammaLabel.setText(locale["Settings/Video/ColorAdjustment/Gamma"]);
  luminanceLabel.setText(locale["Settings/Video/ColorAdjustment/Luminance"]);

  overscanMaskLabel.setText(locale["Settings/Video/OverscanMask"]);
  horizontalMaskLabel.setText(locale["Settings/Video/OverscanMask/Horizontal"]);
  verticalMaskLabel.setText(locale["Settings/Video/OverscanMask/Vertical"]);

  fullscreenLabel.setText(locale["Settings/Video/Fullscreen"]);
  fullscreenExclusive.setText(locale["Settings/Video/Fullscreen/Exclusive"]);
}

auto VideoSettings::updateColor(bool initializing) -> void {
  settings["Video/Saturation"].setValue(saturationSlider.position());
  settings["Video/Gamma"].setValue(100 + gammaSlider.position());
  settings["Video/Luminance"].setValue(luminanceSlider.position());
  saturationValue.setText({saturationSlider.position(), "%"});
  gammaValue.setText({100 + gammaSlider.position(), "%"});
  luminanceValue.setText({luminanceSlider.position(), "%"});

  if(!initializing) program->updateVideoPalette();
}

auto VideoSettings::updateOverscan(bool initializing) -> void {
  settings["View/Overscan/Horizontal"].setValue(horizontalMaskSlider.position());
  settings["View/Overscan/Vertical"].setValue(verticalMaskSlider.position());
  horizontalMaskValue.setText({horizontalMaskSlider.position()});
  verticalMaskValue.setText({verticalMaskSlider.position()});

  if(initializing || settings["View/Overscan"].boolean()) return;
  if(settings["View/Adaptive"].boolean()) {
    presentation->resizeWindow();
  } else {
    presentation->resizeViewport();
  }
}
