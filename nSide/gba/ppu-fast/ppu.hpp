#define PPU PPUfast
#define ppu ppufast

struct PPU : Thread, IO {
  PPU();
  ~PPU();

  static auto Enter() -> void;
  auto step(uint clocks) -> void;
  auto main() -> void;

  auto frame() -> void;
  auto refresh() -> void;
  auto power() -> void;

  auto readIO(uint32 addr) -> uint8;
  auto writeIO(uint32 addr, uint8 byte) -> void;

  auto readVRAM(uint mode, uint32 addr) -> uint32;
  auto writeVRAM(uint mode, uint32 addr, uint32 word) -> void;

  auto readPRAM(uint mode, uint32 addr) -> uint32;
  auto writePRAM(uint mode, uint32 addr, uint32 word) -> void;

  auto readOAM(uint mode, uint32 addr) -> uint32;
  auto writeOAM(uint mode, uint32 addr, uint32 word) -> void;

  auto serialize(serializer&) -> void;

  uint8 vram[96 * 1024];
  uint16 pram[512];

private:
  enum : uint { OBJ = 0, BG0 = 1, BG1 = 2, BG2 = 3, BG3 = 4, SFX = 5 };
  enum : uint { In0 = 0, In1 = 1, Obj = 2, Out = 3 };

  struct IO {
    uint1 gameBoyColorMode;
  } io;

  struct Background {
    struct IO {
      static uint3 mode;
      static uint1 frame;
      static uint5 mosaicWidth;
      static uint5 mosaicHeight;
    } io;
  } bg0, bg1, bg2, bg3;

  struct Objects {
    struct IO {
      uint5 mosaicWidth;
      uint5 mosaicHeight;
    } io;
  } objects;

  struct Registers {
    struct Control {
      uint1 hblank;
      uint1 objmapping;
      uint1 forceblank;
      uint1 enable[5];
      uint1 enablewindow[3];
    } control;

    uint1 greenswap;

    struct Status {
      uint1 vblank;
      uint1 hblank;
      uint1 vcoincidence;
      uint1 irqvblank;
      uint1 irqhblank;
      uint1 irqvcoincidence;
      uint8 vcompare;
    } status;

    uint16 vcounter;

    struct Background {
      struct Control {
        uint2 priority;
        uint2 characterbaseblock;
        uint2 unused;
        uint1 mosaic;
        uint1 colormode;
        uint5 screenbaseblock;
        uint1 affinewrap;  //BG2,3 only
        uint2 screensize;
      } control;
      uint9 hoffset;
      uint9 voffset;

      //BG2,3 only
      int16 pa, pb, pc, pd;
      int28 x, y;

      //internal
      int28 lx, ly;
      uint vmosaic;
      uint hmosaic;
      uint id;
    } bg[4];

    struct Window {
      uint8 x1, x2;
      uint8 y1, y2;
    } window[2];

    struct WindowFlags {
      uint1 enable[6];
    } windowflags[4];

    struct Mosaic {
      uint4 objhsize;
      uint4 objvsize;
    } mosaic;

    struct Blend {
      struct Control {
        uint1 above[6];
        uint2 mode;
        uint1 below[6];
      } control;
      uint5 eva;
      uint5 evb;
      uint5 evy;
    } blend;
  } regs;

  struct Pixel {
    bool   enable;
    uint2  priority;
    uint15 color;

    //objects only
    bool translucent;
    bool mosaic;

    alwaysinline auto write(bool e) { enable = e; }
    alwaysinline auto write(bool e, uint p, uint c) { enable = e; priority = p; color = c; }
    alwaysinline auto write(bool e, uint p, uint c, bool t, bool m) { enable = e; priority = p; color = c; translucent = t; mosaic = m; }
  } layer[6][240];

  bool windowmask[3][240];
  uint vmosaic[5];
  uint hmosaic[5];

  struct Object {
    uint8  y;
    uint1  affine;
    uint1  affineSize;
    uint2  mode;
    uint1  mosaic;
    uint1  colors;      //0 = 16, 1 = 256
    uint2  shape;       //0 = square, 1 = horizontal, 2 = vertical

    uint9  x;
    uint5  affineParam;
    uint1  hflip;
    uint1  vflip;
    uint2  size;

    uint10 character;
    uint2  priority;
    uint4  palette;

    //ancillary data
    uint width;
    uint height;
  } object[128];

  struct ObjectParam {
    int16 pa;
    int16 pb;
    int16 pc;
    int16 pd;
  } objectParam[32];

  struct Tile {
    uint10 character;
    uint1  hflip;
    uint1  vflip;
    uint4  palette;
  };

  auto renderBackgrounds() -> void;
  auto renderBackgroundLinear(Registers::Background&) -> void;
  auto renderBackgroundAffine(Registers::Background&) -> void;
  auto renderBackgroundBitmap(Registers::Background&) -> void;

  auto renderObjects() -> void;
  auto renderObject(Object&) -> void;
  auto readObjectVRAM(uint addr) const -> uint8;

  auto renderMosaicBackground(uint id) -> void;
  auto renderMosaicObject() -> void;

  auto renderForceBlank() -> void;
  auto renderScreen() -> void;
  auto renderWindow(uint window) -> void;
  auto blend(uint above, uint eva, uint below, uint evb) -> uint;
};

extern PPU ppu;

#undef PPU
#undef ppu
