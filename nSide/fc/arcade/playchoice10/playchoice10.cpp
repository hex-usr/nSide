#include <fc/fc.hpp>

namespace higan::Famicom {

PlayChoice10 playchoice10;

#include "cpu.cpp"
#include "video-circuit.cpp"
#include "serialization.cpp"

auto PlayChoice10::load() -> bool {
  string firmware = property.pc10.screens() == 1 ? "bios-single.rom" : "bios-dual.rom";
  if(auto fp = platform->open(ID::System, firmware, File::Read, File::Required)) {
    fp->read(bios, 16384);
  } else return false;

  if(auto fp = platform->open(ID::System, "character.rom", File::Read, File::Required)) {
    fp->read(videoCircuit.chrrom, 24576);
  } else return false;

  if(auto fp = platform->open(ID::System, "palette.rom", File::Read, File::Required)) {
    fp->read(videoCircuit.cgrom, 768);
  } else return false;

  dip = platform->dipSettings(BML::unserialize(
    "setting name:Seconds|coin (1 | 2)\n"
    "  option value=0x0000 name:300 | 0\n"
    "  option value=0x0001 name:300 | 100\n"
    "  option value=0x0002 name:300 | 200\n"
    "  option value=0x0003 name:300 | 300\n"
    "  option value=0x0004 name:300 | 400\n"
    "  option value=0x0005 name:300 | 500\n"
    "  option value=0x0006 name:300 | 600\n"
    "  option value=0x0007 name:300 | 700\n"
    "  option value=0x0008 name:300 | 800\n"
    "  option value=0x0009 name:300 | 900\n"
    "  option value=0x000a name:150 | 0\n"
    "  option value=0x000b name:150 | 200\n"
    "  option value=0x000c name:150 | 400\n"
    "  option value=0x000d name:150 | 500\n"
    "  option value=0x000e name:150 | 600\n"
    "  option value=0x000f name:150 | 800\n"
    "  option value=0x0010 name:300 | 1000\n"
    "  option value=0x0011 name:300 | 1100\n"
    "  option value=0x0012 name:300 | 1200\n"
    "  option value=0x0013 name:300 | 1300\n"
    "  option value=0x0014 name:300 | 1400\n"
    "  option value=0x0015 name:300 | 1500\n"
    "  option value=0x0016 name:300 | 1600\n"
    "  option value=0x0017 name:300 | 1700\n"
    "  option value=0x0018 name:300 | 1800\n"
    "  option value=0x0019 name:300 | 1900\n"
    "  option value=0x001a name:150 | 1000\n"
    "  option value=0x001b name:150 | 1200\n"
    "  option value=0x001c name:150 | 1400\n"
    "  option value=0x001d name:150 | 1500\n"
    "  option value=0x001e name:150 | 1600\n"
    "  option value=0x001f name:150 | 1800\n"
    "  option value=0x0020 name:300 | 2000\n"
    "  option value=0x0021 name:300 | 2100\n"
    "  option value=0x0022 name:300 | 2200\n"
    "  option value=0x0023 name:300 | 2300\n"
    "  option value=0x0024 name:300 | 2400\n"
    "  option value=0x0025 name:300 | 2500\n"
    "  option value=0x0026 name:300 | 2600\n"
    "  option value=0x0027 name:300 | 2700\n"
    "  option value=0x0028 name:300 | 2800\n"
    "  option value=0x0029 name:300 | 2900\n"
    "  option value=0x002a name:150 | 2000\n"
    "  option value=0x002b name:150 | 2200\n"
    "  option value=0x002c name:150 | 2400\n"
    "  option value=0x002d name:150 | 2500\n"
    "  option value=0x002e name:150 | 2600\n"
    "  option value=0x002f name:150 | 2800\n"
    "  option value=0x0030 name:300 | 3000\n"
    "  option value=0x0031 name:300 | 3100\n"
    "  option value=0x0032 name:300 | 3200\n"
    "  option value=0x0033 name:300 | 3300\n"
    "  option value=0x0034 name:300 | 3400\n"
    "  option value=0x0035 name:300 | 3500\n"
    "  option value=0x0036 name:300 | 3600\n"
    "  option value=0x0037 name:300 | 3700\n"
    "  option value=0x0038 name:300 | 3800\n"
    "  option value=0x0039 name:300 | 3900\n"
    "  option value=0x003a name:150 | 3000\n"
    "  option value=0x003b name:150 | 3200\n"
    "  option value=0x003c name:150 | 3400\n"
    "  option value=0x003d name:150 | 3500\n"
    "  option value=0x003e name:150 | 3600\n"
    "  option value=0x003f name:150 | 3800\n"
    "setting name:Attract Mode Audio\n"
    "  option value=0x0000 name:Muted\n"
    "  option value=0x0040 name:Plays\n"
    "setting name:Service Mode\n"
    "  option value=0x0000 name:Normal\n"
    "  option value=0x0080 name:Service Mode\n"
    "setting name:Timer Speed\n"
    "  option value=0x3f00 name:1× (5:00)\n"
    "  option value=0x8000 name:Free Play\n"
    "  option value=0x0500 name:30× (0:10)\n"
    "  option value=0x0600 name:20× (0:15)\n"
    "  option value=0x0700 name:15× (0:20)\n"
    "  option value=0x0800 name:12× (0:25)\n"
    "  option value=0x0900 name:10× (0:30)\n"
    "  option value=0x0a00 name:60/7× (0:35)\n"
    "  option value=0x0b00 name:15/2× (0:40)\n"
    "  option value=0x0c00 name:20/3× (0:45)\n"
    "  option value=0x0d00 name:6× (0:50)\n"
    "  option value=0x0e00 name:60/11× (0:55)\n"
    "  option value=0x0f00 name:5× (1:00)\n"
    "  option value=0x1000 name:60/13× (1:05)\n"
    "  option value=0x1100 name:30/7× (1:10)\n"
    "  option value=0x1200 name:4× (1:15)\n"
    "  option value=0x1300 name:15/4× (1:20)\n"
    "  option value=0x1400 name:60/17× (1:25)\n"
    "  option value=0x1500 name:10/3× (1:30)\n"
    "  option value=0x1600 name:60/19× (1:35)\n"
    "  option value=0x1700 name:3× (1:40)\n"
    "  option value=0x1800 name:20/7× (1:45)\n"
    "  option value=0x1900 name:30/11× (1:50)\n"
    "  option value=0x1a00 name:60/23× (1:55)\n"
    "  option value=0x1b00 name:5/2× (2:00)\n"
    "  option value=0x1c00 name:12/5× (2:05)\n"
    "  option value=0x1d00 name:30/13× (2:10)\n"
    "  option value=0x1e00 name:20/9× (2:15)\n"
    "  option value=0x1f00 name:15/7× (2:20)\n"
    "  option value=0x2000 name:60/29× (2:25)\n"
    "  option value=0x2100 name:2× (2:30)\n"
    "  option value=0x2200 name:60/31× (2:35)\n"
    "  option value=0x2300 name:15/8× (2:40)\n"
    "  option value=0x2400 name:20/11× (2:45)\n"
    "  option value=0x2500 name:30/17× (2:50)\n"
    "  option value=0x2600 name:12/7× (2:55)\n"
    "  option value=0x2700 name:5/3× (3:00)\n"
    "  option value=0x2800 name:60/37× (3:05)\n"
    "  option value=0x2900 name:30/19× (3:10)\n"
    "  option value=0x2a00 name:20/13× (3:15)\n"
    "  option value=0x2b00 name:3/2× (3:20)\n"
    "  option value=0x2c00 name:60/41× (3:25)\n"
    "  option value=0x2d00 name:10/7× (3:30)\n"
    "  option value=0x2e00 name:60/43× (3:35)\n"
    "  option value=0x2f00 name:15/11× (3:40)\n"
    "  option value=0x3000 name:4/3× (3:45)\n"
    "  option value=0x3100 name:30/23× (3:50)\n"
    "  option value=0x3200 name:60/47× (3:55)\n"
    "  option value=0x3300 name:5/4× (4:00)\n"
    "  option value=0x3400 name:60/49× (4:05)\n"
    "  option value=0x3500 name:6/5× (4:10)\n"
    "  option value=0x3600 name:20/17× (4:15)\n"
    "  option value=0x3700 name:15/13× (4:20)\n"
    "  option value=0x3800 name:60/53× (4:25)\n"
    "  option value=0x3900 name:10/9× (4:30)\n"
    "  option value=0x3a00 name:12/11× (4:35)\n"
    "  option value=0x3b00 name:15/14× (4:40)\n"
    "  option value=0x3c00 name:20/19× (4:45)\n"
    "  option value=0x3d00 name:30/29× (4:50)\n"
    "  option value=0x3e00 name:60/59× (4:55)\n"
    "  option value=0x0000 name:60/253× (21:05)\n"
    "  option value=0x0100 name:30/127× (21:10)\n"
    "  option value=0x0200 name:4/17× (21:15)\n"
    "  option value=0x0300 name:15/64× (21:20)\n"
    "  option value=0x0400 name:60/257× (21:25)\n"
    "setting name:Slot B Coin Divisor\n"
    "  option value=0x0000 name:÷ 1\n"
    "  option value=0x4000 name:÷ 2\n"
  ));

  return true;
}

auto PlayChoice10::power(bool reset) -> void {
  pc10cpu.power(reset);
  videoCircuit.power(reset);

  if(!reset) {
    function<auto (uint16, uint8) -> uint8> reader;
    function<auto (uint16, uint8) -> void> writer;

    reader = {&PlayChoice10::readController1, this};
    writer = {&PlayChoice10::latchControllers, this};
    busM.map(reader, writer, "4016-4016");

    nmiDetected = false;

    vramAccess      = 0;  //0: Z80,                  1: video circuit
    gameSelectStart = 0;  //0: disable START/SELECT, 1: enable START/SELECT
    ppuOutput       = 0;  //0: disable,              1: enable
    apuOutput       = 0;  //0: disable,              1: enable
    cpuReset        = 0;  //0: reset,                1: run
    cpuStop         = 0;  //0: stop,                 1: run
    display         = 0;  //0: video circuit,        1: PPU
    z80NMI          = 0;  //0: disable,              1: enable
    watchdog        = 0;  //0: enable,               1: disable
    ppuReset        = 0;  //0: reset,                1: run

    random.array(wram, sizeof(wram));

    channel  = 0;  //channel 1 on-screen
    sramBank = 1;

    promAddress = 0;
  }
}

auto PlayChoice10::read(uint16 addr) -> uint8 {
  if(addr < 0x8000) return bios[addr & 0x3fff];
  if(addr < 0x8800) return wram[addr & 0x07ff];
  if(addr < 0x8c00) return sram[addr & 0x03ff];
  if(addr < 0x9000) return sram[(addr & 0x03ff) | (sramBank << 10)];
  if(addr < 0x9800) return 0x00;  //VRAM is write-only
  if(addr < 0xc000) return 0x00;  //open bus
  if(channel >= cartridgeSlot.size()) return 0xff;
  if(addr < 0xe000) return cartridgeSlot[busM.slot].board->instrom.read(addr & 0x1fff);

  //PROM
  uint8 data = 0xe7;
  uint8 byte;
  if(!promTest || !promAddress.bit(6)) {
    byte = cartridgeSlot[busM.slot].board->keyrom.read((promAddress & 0x3f) >> 3);
  } else {
    byte = promAddress.bit(4) ? (uint8)0x00 : cartridgeSlot[busM.slot].board->keyrom.read(8);
  }
  data.bit(3) = !byte.bit(promAddress & 7);
  data.bit(4) = !promAddress.bit(5);
  return data;
}

auto PlayChoice10::write(uint16 addr, uint8 data) -> void {
  if(addr < 0x8000) return;
  if(addr < 0x8800) { wram[addr & 0x07ff] = data; return; }
  if(addr < 0x8c00) { sram[addr & 0x03ff] = data; return; }
  if(addr < 0x9000) { sram[(addr & 0x03ff) | (sramBank << 10)] = data; return; }
  if(addr < 0x9800) { videoCircuit.writeVRAM(addr, data); return; }
  if(addr < 0xc000) return;
  if(addr < 0xe000) return;

  promTest = data.bit(4);
  if(promClock && data.bit(3) == 0) promAddress++;
  promClock = data.bit(3);
  if(data.bit(0) == 0) promAddress = 0;
  return;
}

auto PlayChoice10::in(uint8 addr) -> uint8 {
  bool channelSelect = poll(ChannelSelect);
  bool enter         = poll(Enter);
  bool reset         = poll(Reset);
  bool nmi           = !nmiDetected;
  bool primeTime     = false;
  bool coin2         = poll(Coin2);
  bool service       = poll(ServiceButton);
  bool coin1         = poll(Coin1);
  switch(addr & 0x03) {
  case 0x00:
    return channelSelect << 0 | enter << 1 | reset << 2 | nmi << 3
    | primeTime << 4 | coin2 << 5 | service << 6 | coin1 << 7;
  case 0x01: return dip.byte(0);
  case 0x02: return dip.byte(1);
  case 0x03: return nmiDetected = false, 0x00;
  }
  unreachable;
}

auto PlayChoice10::out(uint8 addr, uint8 data) -> void {
  data &= 0x01;
  switch(addr & 0x1f) {
  case 0x00: {
    vramAccess = data;
    break;
  }
  case 0x01: {
    gameSelectStart = data;
    break;
  }
  case 0x02: {
    ppuOutput = data;
    break;
  }
  case 0x03: {
    apuOutput = data;
    break;
  }
  case 0x04: {
    if(!cpuReset && data) {
      cpuM.power(/* reset = */ true);
      apuM.power(/* reset = */ true);
    };
    cpuReset = data;
    break;
  }
  case 0x05: {
    cpuStop = data;
    print(data ? "Start" : "Stop", " CPU\n");
    break;
  }
  case 0x06: {
    display = data;
    break;
  }
  case 0x08: {
    z80NMI = data;
    break;
  }
  case 0x09: {
    watchdog = data;
    print(data ? "Enable" : "Disable", " Watchdog\n");
    break;
  }
  case 0x0a: {
    if(!ppuReset && data) ppuM.power(/* reset = */ true);
    ppuReset = data;
    break;
  }
  case 0x0b: {
    uint4 newChannel = channel;
    newChannel.bit(0) = data;
    changeChannel(newChannel);
    break;
  }
  case 0x0c: {
    uint4 newChannel = channel;
    newChannel.bit(1) = data;
    changeChannel(newChannel);
    break;
  }
  case 0x0d: {
    uint4 newChannel = channel;
    newChannel.bit(2) = data;
    changeChannel(newChannel);
    break;
  }
  case 0x0e: {
    uint4 newChannel = channel;
    newChannel.bit(3) = data;
    changeChannel(newChannel);
    break;
  }
  case 0x0f: {
    sramBank = data;
    break;
  }

  }
  switch(addr & 0x13) {
  case 0x10: break;
  case 0x11: break;
  case 0x12: break;
  case 0x13: break;
  }
}

auto PlayChoice10::changeChannel(uint4 newChannel) -> void {
  if(newChannel == channel) return;
  scheduler.remove(cartridgeSlot[channel]);
  channel = newChannel;
  if(channel < cartridgeSlot.size()) busM.slot = channel;
  cartridgeSlot[busM.slot].power(false);
}

auto PlayChoice10::poll(uint input) -> int16 {
  return platform->inputPoll(ID::Port::Hardware, ID::Device::PlayChoice10Controls, (uint)input);
}

auto PlayChoice10::readController1(uint16 addr, uint8 data) -> uint8 {
  auto gamepad = static_cast<Gamepad*>(controllerPortM1.device);
  uint counter = gamepad->counter;
  uint8 input = cpuM.readCPU(addr, data);
  switch(gamepad->latched ? 0 : counter) {
  case 2: data.bit(0) = controller1GameSelect; break;
  case 3: data.bit(0) = controller1Start; break;
  default: data.bit(0) = input.bit(0); break;
  }
  return data;
}

auto PlayChoice10::latchControllers(uint16 addr, uint8 data) -> void {
  auto gamepad = static_cast<Gamepad*>(controllerPortM1.device);
  bool old_latched = gamepad->latched;
  cpuM.writeCPU(addr, data);
  if(old_latched == data.bit(0)) return;
  if(gamepad->latched == 0) {
    controller1GameSelect = gameSelectStart & poll(GameSelect);
    controller1Start      = gameSelectStart & poll(Start);
  }
}

}
