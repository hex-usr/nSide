auto VSSystem::serialize(serializer& s) -> void {
  ram.serialize(s);
  s.integer(ramSide);

  s.integer(dipM);
  s.integer(dipS);
  s.integer(watchdog);
}
