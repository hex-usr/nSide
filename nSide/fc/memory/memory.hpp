struct Bus {
  alwaysinline static auto mirror(uint address, uint size) -> uint;
  alwaysinline static auto reduce(uint address, uint mask) -> uint;

  Bus(uint slot_);
  ~Bus();

  alwaysinline auto read(uint16 address, uint8 data) -> uint8;
  alwaysinline auto write(uint16 address, uint8 data) -> void;

  alwaysinline auto readCHR(uint16 address, uint8 data) -> uint8;
  alwaysinline auto writeCHR(uint16 address, uint8 data) -> void;

  auto reset() -> void;
  auto map(
    const function<uint8 (uint16, uint8)>& read,
    const function<void (uint16, uint8)>& write,
    const string& address, uint size = 0, uint base = 0, uint mask = 0
  ) -> uint;
  auto unmap(const string& address) -> void;

  //Famicom: always 0
  //VS. System: 0=main, 1=sub
  //PlayChoice-10: cartridge slot ID (0-9)
  //FamicomBox: cartridge slot ID (0-14)
  uint slot;

private:
  uint8* lookup = nullptr;
  uint32* target = nullptr;

  function<uint8 (uint16, uint8)> reader[256];
  function<void (uint16, uint8)> writer[256];
  uint16 counter[256];
};

extern Bus busM;
extern Bus busS;
