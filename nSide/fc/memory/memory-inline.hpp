auto Bus::mirror(uint address, uint size) -> uint {
  if(size == 0) return 0;
  uint base = 0;
  uint mask = 1 << 15;
  while(address >= size) {
    while(!(address & mask)) mask >>= 1;
    address -= mask;
    if(size > mask) {
      size -= mask;
      base += mask;
    }
    mask >>= 1;
  }
  return base + address;
}

auto Bus::reduce(uint address, uint mask) -> uint {
  while(mask) {
    uint bits = (mask & -mask) - 1;
    address = ((address >> 1) & ~bits) | (address & bits);
    mask = (mask & (mask - 1)) >> 1;
  }
  return address;
}

//$0000-07ff = RAM (2KB)
//$0800-1fff = RAM (mirror)
//$2000-2007 = PPU
//$2008-3fff = PPU (mirror)
//$4000-4017 = APU + I/O
//$4018-ffff = Cartridge

auto Bus::read(uint16 address, uint8 data) -> uint8 {
  if(!Model::FamicomBox()) data = cartridgeSlot[slot].readPRG(address, data);
  data = reader[lookup[address]](target[address], data);
  if(cheat) {
    if(auto result = cheat.find(address, data)) return result();
  }
  return data;
}

auto Bus::write(uint16 address, uint8 data) -> void {
  if(!Model::FamicomBox()) cartridgeSlot[slot].writePRG(address, data);
  return writer[lookup[address]](target[address], data);
}

auto Bus::readCHR(uint16 address, uint8 data) -> uint8 {
  if(Model::FamicomBox()) return famicombox.readCHR(address, data);
  return cartridgeSlot[slot].readCHR(address, data);
}

auto Bus::writeCHR(uint16 address, uint8 data) -> void {
  if(Model::FamicomBox()) return famicombox.writeCHR(address, data);
  return cartridgeSlot[slot].writeCHR(address, data);
}
