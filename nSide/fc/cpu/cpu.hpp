struct CPU : MOS6502, Thread {
  inline auto rate() const -> uint { return Region::PAL() ? 16 : Region::Dendy() ? 15 : 12; }

  CPU(bool side);

  static auto Enter() -> void;
  auto main() -> void;
  auto load() -> bool;
  auto power(bool reset) -> void;

  //memory.cpp
  auto read(uint16 addr) -> uint8 override;
  auto write(uint16 addr, uint8 data) -> void override;
  auto readDebugger(uint16 addr) -> uint8 override;

  //mmio.cpp
  auto readCPU(uint16 addr, uint8 data) -> uint8;
  auto writeCPU(uint16 addr, uint8 data) -> void;

  //timing.cpp
  auto step(uint clocks) -> void;
  auto lastCycle() -> void;
  auto nmi(uint16 &vector) -> void;

  auto oamdma() -> void;

  auto nmiLine(bool) -> void;
  auto irqLine(bool) -> void;
  auto apuLine(bool) -> void;

  auto rdyLine(bool) -> void;
  auto rdyAddr(bool valid, uint16 value = 0) -> void;

  //serialization.cpp
  auto serialize(serializer&) -> void;

  Memory::Writable<uint8> ram;
  vector<Thread*> coprocessors;
  vector<Thread*> peripherals;

  const bool side;  //0: main, 1: sub (VS. System only)

//private:
  struct IO {
    bool interruptPending = 0;
    bool nmiPending = 0;
    bool nmiLine = 0;
    bool irqLine = 0;
    bool apuLine = 0;

    bool rdyLine = 1;
    bool rdyAddrValid = 0;
    uint16 rdyAddrValue;

    bool oamdmaPending = 0;
    uint8 oamdmaPage;
  } io;
};

extern CPU cpuM;
extern CPU cpuS;
