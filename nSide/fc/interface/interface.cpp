#include <fc/fc.hpp>

namespace higan::Famicom {

Options option;
Properties property;
#include "famicom.cpp"
#include "vs-system.cpp"
#include "playchoice-10.cpp"
#include "famicombox.cpp"

auto AbstractInterface::loaded() -> bool {
  return system.loaded();
}

auto AbstractInterface::hashes() -> vector<string> {
  return cartridgeSlot[Model::VSSystem()].hashes();
}

auto AbstractInterface::manifests() -> vector<string> {
  return cartridgeSlot[Model::VSSystem()].manifests();
}

auto AbstractInterface::titles() -> vector<string> {
  return cartridgeSlot[Model::VSSystem()].titles();
}

auto AbstractInterface::save() -> void {
  system.save();
}

auto AbstractInterface::unload() -> void {
  save();
  system.unload();
}

auto AbstractInterface::connected(uint port) -> uint {
  if(port == ID::Port::Controller1) return option.port.controller1.device();
  if(port == ID::Port::Controller2) return option.port.controller2.device();
  if(port == ID::Port::Expansion) return option.port.expansion.device();
  return 0;
}

auto AbstractInterface::power() -> void {
  system.power(/* reset = */ false);
}

auto AbstractInterface::reset() -> void {
  system.power(/* reset = */ true);
}

auto AbstractInterface::run() -> void {
  system.run();
}

auto AbstractInterface::serialize() -> serializer {
  system.runToSave();
  return system.serialize();
}

auto AbstractInterface::unserialize(serializer& s) -> bool {
  return system.unserialize(s);
}

auto AbstractInterface::cheats(const vector<string>& list) -> void {
  cheat.reset();
  cheat.assign(list);
}

auto AbstractInterface::options() -> Settings& {
  return option;
}

auto AbstractInterface::properties() -> Settings& {
  return property;
}

}
