auto FamicomInterface::information() -> Information {
  Information information;
  information.manufacturer = "Nintendo";
  information.name         = "Famicom";
  information.extension    = "fc";
  information.resettable   = true;
  return information;
}

auto FamicomInterface::display() -> Display {
  double squarePixelRate = Famicom::Region::NTSCJ() || Famicom::Region::NTSCU()
  ? 135.0 / 22.0 * 1'000'000.0
  : 7'375'000.0;

  Display display;
  display.type   = Display::Type::CRT;
  display.colors = 1 << 9;
  display.width  = 256;
  display.height = 240;
  display.internalWidth  = 256;
  display.internalHeight = 240;
  display.aspectCorrection = squarePixelRate / (system.frequency() / ppuM.rate());
  return display;
}

auto FamicomInterface::color(uint32 color) -> uint64 {
  //YIQ decoder by Bisqwit (http://forums.nesdev.com/viewtopic.php?p=85060)
  static auto generateNTSCColor = [](uint9 n, double saturation, double hue, double contrast, double brightness, double gamma) -> uint64 {
    uint4 color = n.bits(0,3);
    uint2 level = color >= 0xe ? 1 : n.bits(4,5);

    static const double  lowLevels[4] = {0.350, 0.518, 0.962, 1.550};
    static const double highLevels[4] = {1.094, 1.506, 1.962, 1.962};

    static const double black = lowLevels[1], white = highLevels[3];
    static const double attenuation = 0.746;

    double lo_and_hi[2] = {
      (color == 0x0 ? highLevels : lowLevels)[level],
      (color <  0xd ? highLevels : lowLevels)[level],
    };

    double y = 0.0, i = 0.0, q = 0.0;
    auto wave = [](int phase, int color) -> bool { return (color + phase + 8) % 12 < 6; };
    for(int phase : range(12)) {
      double spot = lo_and_hi[wave(phase, color)];

      if(color < 0xe && (
         ((n.bit(6)) && wave(phase, 12))
      || ((n.bit(7)) && wave(phase,  4))
      || ((n.bit(8)) && wave(phase,  8))
      )) spot *= attenuation;

      double voltage = (spot - black) / (white - black);

      voltage = (voltage - 0.5) * contrast + 0.5;
      voltage *= brightness / 12.0;

      y += voltage;
      i += voltage * std::cos((Math::Pi / 6.0) * (phase + hue));
      q += voltage * std::sin((Math::Pi / 6.0) * (phase + hue));
    }

    i *= saturation;
    q *= saturation;

    auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
    //This matrix is from FCC's 1953 NTSC standard.
    //The Famicom and American NES are older than the SMPTE C standard that followed in 1987.
    uint64 r = uclamp<16>(65535.0 * gammaAdjust(y +  0.946882 * i +  0.623557 * q));
    uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.274788 * i + -0.635691 * q));
    uint64 b = uclamp<16>(65535.0 * gammaAdjust(y + -1.108545 * i +  1.709007 * q));

    return r << 32 | g << 16 | b << 0;
  };

  static auto generatePALColor = [](uint9 n, double saturation, double hue, double contrast, double brightness, double gamma) -> uint64 {
    uint4 color = n.bits(0,3);
    uint2 level = color >= 0xe ? 1 : n.bits(4,5);

    static const double  lowLevels[4] = {0.350, 0.518, 0.962, 1.550};
    static const double highLevels[4] = {1.094, 1.506, 1.962, 1.962};

    static const double black = lowLevels[1], white = highLevels[3];
    static const double attenuation = 0.746;

    double lo_and_hi[2] = {
      (color == 0x0 ? highLevels : lowLevels)[level],
      (color <  0xd ? highLevels : lowLevels)[level],
    };

    double ys[2] = {0.0, 0.0}, us[2] = {0.0, 0.0}, vs[2] = {0.0, 0.0};
    //Add 12 to work around C++ modulus implementation
    auto wave0 = [](int phase, int color) -> bool { return (color + 10 - phase + 12) % 12 < 6; };
    auto wave1 = [](int phase, int color) -> bool { return (color +  1 + phase +  0) % 12 < 6; };
    for(int phase : range(12)) {
      double spot[] = {lo_and_hi[wave0(phase, color)], lo_and_hi[wave1(phase, color)]};

      //swap red and green
      if(color < 0xe && (
         ((n.bit(6)) && wave0(phase,  4))
      || ((n.bit(7)) && wave0(phase, 12))
      || ((n.bit(8)) && wave0(phase,  8))
      )) spot[0] *= attenuation;

      if(color < 0xe && (
         ((n.bit(6)) && wave1(phase,  4))
      || ((n.bit(7)) && wave1(phase, 12))
      || ((n.bit(8)) && wave1(phase,  8))
      )) spot[1] *= attenuation;

      for(uint i : range(2)) {
        double voltage = (spot[i] - black) / (white - black);

        voltage = (voltage - 0.5) * contrast + 0.5;
        voltage *= brightness / 12.0;

        ys[i] += voltage;
        us[i] += voltage * std::cos((Math::Pi / 6.0) * (phase + hue));
        vs[i] += voltage * std::sin((Math::Pi / 6.0) * (phase + hue));
      }
    }

    double y = (ys[0] + ys[1]) / 2.0, u = (us[0] + us[1]) / 2.0, v = (vs[0] - vs[1]) / 2.0;

    u *= saturation;
    v *= saturation;

    auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
    uint64 r = uclamp<16>(65535.0 * gammaAdjust(y                 +  1.139837 * v));
    uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.394652 * u + -0.580599 * v));
    uint64 b = uclamp<16>(65535.0 * gammaAdjust(y +  2.032110 * u                ));

    return r << 32 | g << 16 | b << 0;
  };

  static auto generateRGBColor = [](uint9 color, const uint9* palette) -> uint64 {
    uint3 r = color.bit(6) ? 7 : palette[color.bits(5,0)].bits(6,8);
    uint3 g = color.bit(7) ? 7 : palette[color.bits(5,0)].bits(3,5);
    uint3 b = color.bit(8) ? 7 : palette[color.bits(5,0)].bits(0,2);

    uint64 R = image::normalize(r, 3, 16);
    uint64 G = image::normalize(g, 3, 16);
    uint64 B = image::normalize(b, 3, 16);

    if(option.video.colorEmulation()) {
      //TODO: check how arcade displays alter the signal.
      //The red, green, and blue channels are connected directly without any
      //conversion to YIQ/YUV/YPbPr/etc. and back.
      static const uint8 gammaRamp[8] = {
        0x00, 0x0a,
        0x2d, 0x5b,
        0x98, 0xb8,
        0xe0, 0xff,
      };
      R = gammaRamp[r] * 0x0101;
      G = gammaRamp[g] * 0x0101;
      B = gammaRamp[b] * 0x0101;
    }

    return R << 32 | G << 16 | B << 0;
  };

  if(ppuM.ntsc()) {
    double saturation = 2.0;
    double hue = 0.0;
    double contrast = 1.0;
    double brightness = 1.0;
    double gamma = option.video.colorEmulation() ? 1.8 : 2.2;

    return generateNTSCColor(color & 0x1ff, saturation, hue, contrast, brightness, gamma);

  } else if(ppuM.pal()) {
    double saturation = 2.0;
    double hue = 0.0;
    double contrast = 1.0;
    double brightness = 1.0;
    double gamma = option.video.colorEmulation() ? 1.8 : 2.2;

    return generatePALColor(color & 0x1ff, saturation, hue, contrast, brightness, gamma);

  } else if(ppuM.rgb()) {
    const uint9* palette = nullptr;
    switch(ppuM.version) {
    case PPU::Version::RP2C03B:
    case PPU::Version::RP2C03G:
    case PPU::Version::RC2C03B:
    case PPU::Version::RC2C03C:
    case PPU::Version::RC2C05_01:
    case PPU::Version::RC2C05_02:
    case PPU::Version::RC2C05_03:
    case PPU::Version::RC2C05_04:
    case PPU::Version::RC2C05_05:
      palette = PPU::RP2C03;
      break;
    case PPU::Version::RP2C04_0001:
      palette = PPU::RP2C04_0001;
      break;
    case PPU::Version::RP2C04_0002:
      palette = PPU::RP2C04_0002;
      break;
    case PPU::Version::RP2C04_0003:
      palette = PPU::RP2C04_0003;
      break;
    case PPU::Version::RP2C04_0004:
      palette = PPU::RP2C04_0004;
      break;
    }
    return generateRGBColor(color & 0x1ff, palette);
  }

  return 0;
}

auto FamicomInterface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Expansion,   "Expansion Port"   }};
}

auto FamicomInterface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::None,        "None"        },
    {ID::Device::Gamepad,     "Gamepad"     },
    {ID::Device::FourScore1,  "Four Score"  },
    {ID::Device::SNESGamepad, "SNES Gamepad"},
    {ID::Device::Mouse,       "Mouse"       }
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::None,        "None"         },
    {ID::Device::Gamepad,     "Gamepad"      },
    {ID::Device::GamepadMic,  "Gamepad Mic"  },
    {ID::Device::FourScore2,  "Four Score"   },
    {ID::Device::Zapper,      "Zapper"       },
    {ID::Device::PowerPad,    "Power Pad"    },
    {ID::Device::Vaus,        "Arkanoid Vaus"},
    {ID::Device::SNESGamepad, "SNES Gamepad" },
    {ID::Device::Mouse,       "Mouse"        }
  };

  if(port == ID::Port::Expansion) return {
    {ID::Device::None,           "None"                 },
    {ID::Device::GamepadE,       "Gamepad"              },
    {ID::Device::JoyPair,        "JoyPair"              },
    {ID::Device::FourPlayers,    "4-Players Adaptor"    },
    {ID::Device::BeamGun,        "Beam Gun"             },
    {ID::Device::FamilyKeyboard, "Family BASIC Keyboard"},
    {ID::Device::FamilyTrainer,  "Family Trainer"       },
    {ID::Device::VausE,          "Arkanoid Vaus"        },
    {ID::Device::SFCGamepad,     "SFC Gamepad"          },
    {ID::Device::MouseE,         "Mouse"                }
  };

  return {};
}

auto FamicomInterface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::Gamepad
  || device == ID::Device::GamepadE) return {
    {Type::Hat,     "Up"    },
    {Type::Hat,     "Down"  },
    {Type::Hat,     "Left"  },
    {Type::Hat,     "Right" },
    {Type::Button,  "B"     },
    {Type::Button,  "A"     },
    {Type::Control, "Select"},
    {Type::Control, "Start" }
  };

  if(device == ID::Device::GamepadMic) return {
    {Type::Hat,     "Up"    },
    {Type::Hat,     "Down"  },
    {Type::Hat,     "Left"  },
    {Type::Hat,     "Right" },
    {Type::Button,  "B"     },
    {Type::Button,  "A"     },
    {Type::Control, "Mic"   }
  };

  if(device == ID::Device::FourScore1) {
    vector<Input> inputs;
    for(uint p : {1,3}) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"    }},
      {Type::Hat,     {"Port ", p, " - ", "Down"  }},
      {Type::Hat,     {"Port ", p, " - ", "Left"  }},
      {Type::Hat,     {"Port ", p, " - ", "Right" }},
      {Type::Button,  {"Port ", p, " - ", "B"     }},
      {Type::Button,  {"Port ", p, " - ", "A"     }},
      {Type::Control, {"Port ", p, " - ", "Select"}},
      {Type::Control, {"Port ", p, " - ", "Start" }}
    });
    return inputs;
  }

  if(device == ID::Device::FourScore2) {
    vector<Input> inputs;
    for(uint p : {2,4}) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"    }},
      {Type::Hat,     {"Port ", p, " - ", "Down"  }},
      {Type::Hat,     {"Port ", p, " - ", "Left"  }},
      {Type::Hat,     {"Port ", p, " - ", "Right" }},
      {Type::Button,  {"Port ", p, " - ", "B"     }},
      {Type::Button,  {"Port ", p, " - ", "A"     }},
      {Type::Control, {"Port ", p, " - ", "Select"}},
      {Type::Control, {"Port ", p, " - ", "Start" }}
    });
    return inputs;
  }

  if(device == ID::Device::JoyPair) {
    vector<Input> inputs;
    for(char p : {'3','4'}) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"    }},
      {Type::Hat,     {"Port ", p, " - ", "Down"  }},
      {Type::Hat,     {"Port ", p, " - ", "Left"  }},
      {Type::Hat,     {"Port ", p, " - ", "Right" }},
      {Type::Button,  {"Port ", p, " - ", "B"     }},
      {Type::Button,  {"Port ", p, " - ", "A"     }},
      {Type::Control, {"Port ", p, " - ", "Select"}},
      {Type::Control, {"Port ", p, " - ", "Start" }}
    });
    return inputs;
  }

  if(device == ID::Device::FourPlayers) {
    vector<Input> inputs;
    for(char p : {'1','2','3','4'}) inputs.append({
      {Type::Hat,     {"Port ", p, " - ", "Up"    }},
      {Type::Hat,     {"Port ", p, " - ", "Down"  }},
      {Type::Hat,     {"Port ", p, " - ", "Left"  }},
      {Type::Hat,     {"Port ", p, " - ", "Right" }},
      {Type::Button,  {"Port ", p, " - ", "B"     }},
      {Type::Button,  {"Port ", p, " - ", "A"     }},
      {Type::Control, {"Port ", p, " - ", "Select"}},
      {Type::Control, {"Port ", p, " - ", "Start" }}
    });
    return inputs;
  }

  if(device == ID::Device::BeamGun
  || device == ID::Device::Zapper) return {
    {Type::Axis,    "X-axis" },
    {Type::Axis,    "Y-axis" },
    {Type::Control, "Trigger"}
  };

  if(device == ID::Device::FamilyTrainer
  || device == ID::Device::PowerPad) {
    vector<Input> inputs;
    for(uint n : range(12)) inputs.append(
      {Type::Control, {"Button ", n + 1}}
    );
    return inputs;
  }

  if(device == ID::Device::FamilyKeyboard) return {
    {Type::Control, "F1"},
    {Type::Control, "F2"},
    {Type::Control, "F3"},
    {Type::Control, "F4"},
    {Type::Control, "F5"},
    {Type::Control, "F6"},
    {Type::Control, "F7"},
    {Type::Control, "F8"},
    {Type::Control, "1 - Exclamation Mark"},
    {Type::Control, "2 - Quotation Mark"},
    {Type::Control, "3 - Number Sign"},
    {Type::Control, "4 - Dollar Sign"},
    {Type::Control, "5 - Percent Sign"},
    {Type::Control, "6 - Ampersand"},
    {Type::Control, "7 - Apostrophe"},
    {Type::Control, "8 - Left Parenthesis"},
    {Type::Control, "9 - Right Parenthesis"},
    {Type::Control, "0"},
    {Type::Control, "Hyphen-Minus - Equals Sign"},
    {Type::Control, "Circumflex Accent"},
    {Type::Control, "En Sign"},
    {Type::Control, "STOP"},
    {Type::Control, "ESC"},
    {Type::Control, "Q"},
    {Type::Control, "W"},
    {Type::Control, "E"},
    {Type::Control, "R"},
    {Type::Control, "T"},
    {Type::Control, "Y"},
    {Type::Control, "U"},
    {Type::Control, "I"},
    {Type::Control, "O"},
    {Type::Control, "P"},
    {Type::Control, "Commercial At"},
    {Type::Control, "Left Square Bracket"},
    {Type::Control, "RETURN"},
    {Type::Control, "CTR"},
    {Type::Control, "A"},
    {Type::Control, "S"},
    {Type::Control, "D"},
    {Type::Control, "F"},
    {Type::Control, "G"},
    {Type::Control, "H"},
    {Type::Control, "J"},
    {Type::Control, "K"},
    {Type::Control, "L"},
    {Type::Control, "Semicolon - Plus Sign"},
    {Type::Control, "Colon - Asterisk"},
    {Type::Control, "Right Square Bracket"},
    {Type::Control, "Kana"},
    {Type::Control, "SHIFT Left"},
    {Type::Control, "Z"},
    {Type::Control, "X"},
    {Type::Control, "C"},
    {Type::Control, "V"},
    {Type::Control, "B"},
    {Type::Control, "N"},
    {Type::Control, "M"},
    {Type::Control, "Comma - Less-Than Sign"},
    {Type::Control, "Full Stop - Greater-Than Sign"},
    {Type::Control, "Solidus - Question Mark"},
    {Type::Control, "Kana N - Low Line"},
    {Type::Control, "SHIFT Right"},
    {Type::Control, "GRPH"},
    {Type::Control, "Spacebar"},
    {Type::Control, "CLR HOME"},
    {Type::Control, "INS"},
    {Type::Control, "DEL"},
    {Type::Control, "Up"},
    {Type::Control, "Left"},
    {Type::Control, "Right"},
    {Type::Control, "Down"}
  };

  if(device == ID::Device::Vaus
  || device == ID::Device::VausE) return {
    {Type::Axis,    "Control Knob"},
    {Type::Control, "Fire Button" }
  };

  if(device == ID::Device::SFCGamepad
  || device == ID::Device::SNESGamepad) return {
    {Type::Hat,     "Up"    },
    {Type::Hat,     "Down"  },
    {Type::Hat,     "Left"  },
    {Type::Hat,     "Right" },
    {Type::Button,  "B"     },
    {Type::Button,  "A"     },
    {Type::Button,  "Y"     },
    {Type::Button,  "X"     },
    {Type::Trigger, "L"     },
    {Type::Trigger, "R"     },
    {Type::Control, "Select"},
    {Type::Control, "Start" }
  };

  if(device == ID::Device::Mouse
  || device == ID::Device::MouseE) return {
    {Type::Axis,   "X-axis"},
    {Type::Axis,   "Y-axis"},
    {Type::Button, "Left"  },
    {Type::Button, "Right" }
  };

  return {};
}

auto FamicomInterface::load() -> bool {
  return system.load(this, System::Model::Famicom);
}

auto FamicomInterface::connect(uint port, uint device) -> void {
  if(port == ID::Port::Controller1) controllerPortM1.connect(option.port.controller1.device(device));
  if(port == ID::Port::Controller2) controllerPortM2.connect(option.port.controller2.device(device));
  if(port == ID::Port::Expansion) expansionPort.connect(option.port.expansion.device(device));
}
