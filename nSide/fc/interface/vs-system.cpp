auto VSSystemInterface::information() -> Information {
  Information information;
  information.manufacturer = "Nintendo";
  information.name         = "VS. System";
  information.extension    = "vs";
  information.resettable   = false;
  return information;
}

auto VSSystemInterface::display() -> Display {
  double squarePixelRate = 135.0 / 22.0 * 1'000'000.0;

  Display display;
  display.type   = Display::Type::CRT;
  display.colors = (1 << 9) << 1;
  display.width  = 256;
  display.height = 240 / vssystem.gameCount;
  display.internalWidth  = 256 * vssystem.gameCount;
  display.internalHeight = 240;
  display.aspectCorrection = squarePixelRate / (system.frequency() / ppuS.rate());
  return display;
}

auto VSSystemInterface::color(uint32 color) -> uint64 {
  static auto generateRGBColor = [](uint9 color, const uint9* palette) -> uint64 {
    uint3 r = color.bit(6) ? 7 : palette[color.bits(5,0)].bits(6,8);
    uint3 g = color.bit(7) ? 7 : palette[color.bits(5,0)].bits(3,5);
    uint3 b = color.bit(8) ? 7 : palette[color.bits(5,0)].bits(0,2);

    uint64 R = image::normalize(r, 3, 16);
    uint64 G = image::normalize(g, 3, 16);
    uint64 B = image::normalize(b, 3, 16);

    if(option.video.colorEmulation()) {
      //TODO: check how arcade displays alter the signal.
      //The red, green, and blue channels are connected directly without any
      //conversion to YIQ/YUV/YPbPr/etc. and back.
      static const uint8 gammaRamp[8] = {
        0x00, 0x0a,
        0x2d, 0x5b,
        0x98, 0xb8,
        0xe0, 0xff,
      };
      R = gammaRamp[r] * 0x0101;
      G = gammaRamp[g] * 0x0101;
      B = gammaRamp[b] * 0x0101;
    }

    return R << 32 | G << 16 | B << 0;
  };

  const uint9* palette = nullptr;
  switch((color < (1 << 9) ? ppuM : ppuS).version) {
  case PPU::Version::RP2C03B:
  case PPU::Version::RP2C03G:
  case PPU::Version::RC2C03B:
  case PPU::Version::RC2C03C:
  case PPU::Version::RC2C05_01:
  case PPU::Version::RC2C05_02:
  case PPU::Version::RC2C05_03:
  case PPU::Version::RC2C05_04:
  case PPU::Version::RC2C05_05:
    palette = PPU::RP2C03;
    break;
  case PPU::Version::RP2C04_0001:
    palette = PPU::RP2C04_0001;
    break;
  case PPU::Version::RP2C04_0002:
    palette = PPU::RP2C04_0002;
    break;
  case PPU::Version::RP2C04_0003:
    palette = PPU::RP2C04_0003;
    break;
  case PPU::Version::RP2C04_0004:
    palette = PPU::RP2C04_0004;
    break;
  }
  return palette ? generateRGBColor(color & 0x1ff, palette) : (uint64)0ull;
}

auto VSSystemInterface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Hardware,    "Hardware"         }};
}

auto VSSystemInterface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::Gamepad, "Gamepad"},
    {ID::Device::Zapper,  "Zapper" }
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::Gamepad, "Gamepad"},
    {ID::Device::Zapper,  "Zapper" }
  };

  if(port == ID::Port::Hardware) return {
    {ID::Device::VSSystemControls, "VS. System Controls"}
  };

  return {};
}

auto VSSystemInterface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::Gamepad) return {
    {Type::Hat,     "Up"    },
    {Type::Hat,     "Down"  },
    {Type::Hat,     "Left"  },
    {Type::Hat,     "Right" },
    {Type::Button,  "B"     },
    {Type::Button,  "A"     }
  };

  if(device == ID::Device::Zapper) return {
    {Type::Axis,    "X-axis" },
    {Type::Axis,    "Y-axis" },
    {Type::Control, "Trigger"}
  };

  if(device == ID::Device::VSSystemControls) return {
    {Type::Button,  "Button 1"      },
    {Type::Button,  "Button 2"      },
    {Type::Button,  "Button 3"      },
    {Type::Button,  "Button 4"      },
    {Type::Control, "Service Button"},
    {Type::Control, "Coin 1"        },
    {Type::Control, "Coin 2"        }
  };

  return {};
}

auto VSSystemInterface::load() -> bool {
  return system.load(this, System::Model::VSSystem);
}
