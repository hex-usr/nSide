auto PlayChoice10Interface::information() -> Information {
  Information information;
  information.manufacturer = "Nintendo";
  information.name         = "PlayChoice-10";
  information.extension    = "pc10";
  information.resettable   = false;
  return information;
}

auto PlayChoice10Interface::display() -> Display {
  double squarePixelRate = 135.0 / 22.0 * 1'000'000.0;

  Display display;
  display.type   = Display::Type::CRT;
  display.colors = (1 << 9) + (1 << 8);
  display.width  = 256 / property.pc10.screens();
  display.height = property.pc10.screens() == PlayChoice10::ScreenConfig::Dual ? (240 + 224) / 2 : 240;
  display.internalWidth  = 256;
  display.internalHeight = 240 + 224 * (property.pc10.screens() - 1);
  display.aspectCorrection = squarePixelRate / (system.frequency() / ppuM.rate());
  return display;
}

auto PlayChoice10Interface::color(uint32 color) -> uint64 {
  static auto generateRGBColor = [](uint9 color, const uint9* palette) -> uint64 {
    uint3 r = color.bit(6) ? 7 : palette[color.bits(5,0)].bits(6,8);
    uint3 g = color.bit(7) ? 7 : palette[color.bits(5,0)].bits(3,5);
    uint3 b = color.bit(8) ? 7 : palette[color.bits(5,0)].bits(0,2);

    uint64 R = image::normalize(r, 3, 16);
    uint64 G = image::normalize(g, 3, 16);
    uint64 B = image::normalize(b, 3, 16);

    if(option.video.colorEmulation()) {
      //TODO: check how arcade displays alter the signal.
      //The red, green, and blue channels are connected directly without any
      //conversion to YIQ/YUV/YPbPr/etc. and back.
      static const uint8 gammaRamp[8] = {
        0x00, 0x0a,
        0x2d, 0x5b,
        0x98, 0xb8,
        0xe0, 0xff,
      };
      R = gammaRamp[r] * 0x0101;
      G = gammaRamp[g] * 0x0101;
      B = gammaRamp[b] * 0x0101;
    }

    return R << 32 | G << 16 | B << 0;
  };

  static auto generatePC10Color = [](uint9 color) -> uint64 {
    uint r = 15 - playchoice10.videoCircuit.cgrom[color + 0x000];
    uint g = 15 - playchoice10.videoCircuit.cgrom[color + 0x100];
    uint b = 15 - playchoice10.videoCircuit.cgrom[color + 0x200];

    uint64 R = image::normalize(r, 4, 16);
    uint64 G = image::normalize(g, 4, 16);
    uint64 B = image::normalize(b, 4, 16);

    if(option.video.colorEmulation()) {
      //TODO: check the menu monitor's gamma ramp
      static const uint8 gammaRamp[16] = {
        0x00, 0x03, 0x0a, 0x15,
        0x24, 0x37, 0x4e, 0x69,
        0x90, 0xa0, 0xb0, 0xc0,
        0xd0, 0xe0, 0xf0, 0xff,
      };
      R = gammaRamp[r] * 0x0101;
      G = gammaRamp[g] * 0x0101;
      B = gammaRamp[b] * 0x0101;
    }

    return R << 32 | G << 16 | B << 0;
  };

  if(color < (1 << 9)) {
    const uint9* palette = nullptr;
    switch(ppuM.version) {
    case PPU::Version::RP2C03B:
    case PPU::Version::RP2C03G:
    case PPU::Version::RC2C03B:
    case PPU::Version::RC2C03C:
    case PPU::Version::RC2C05_01:
    case PPU::Version::RC2C05_02:
    case PPU::Version::RC2C05_03:
    case PPU::Version::RC2C05_04:
    case PPU::Version::RC2C05_05:
      palette = PPU::RP2C03;
      break;
    case PPU::Version::RP2C04_0001:
      palette = PPU::RP2C04_0001;
      break;
    case PPU::Version::RP2C04_0002:
      palette = PPU::RP2C04_0002;
      break;
    case PPU::Version::RP2C04_0003:
      palette = PPU::RP2C04_0003;
      break;
    case PPU::Version::RP2C04_0004:
      palette = PPU::RP2C04_0004;
      break;
    }
    return generateRGBColor(color & 0x1ff, palette);
  } else {
    return generatePC10Color(color - (1 << 9));
  }
}

auto PlayChoice10Interface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Expansion,   "Expansion Port"   },
  {ID::Port::Hardware,    "Hardware"         }};
}

auto PlayChoice10Interface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::Gamepad, "Gamepad"}
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::Gamepad, "Gamepad"}
  };

  if(port == ID::Port::Expansion) return {
    {ID::Device::None,    "None"  },
    {ID::Device::BeamGun, "Zapper"}
  };

  if(port == ID::Port::Hardware) return {
    {ID::Device::PlayChoice10Controls, "PlayChoice-10 Controls"}
  };

  return {};
}

auto PlayChoice10Interface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::Gamepad) return {
    {Type::Hat,     "Up"    },
    {Type::Hat,     "Down"  },
    {Type::Hat,     "Left"  },
    {Type::Hat,     "Right" },
    {Type::Button,  "B"     },
    {Type::Button,  "A"     }
  };

  if(device == ID::Device::BeamGun) return {
    {Type::Axis,    "X-axis" },
    {Type::Axis,    "Y-axis" },
    {Type::Control, "Trigger"}
  };

  if(device == ID::Device::PlayChoice10Controls) return {
    {Type::Control, "Game Select"   },
    {Type::Control, "Start"         },
    {Type::Control, "Channel Select"},
    {Type::Control, "Enter"         },
    {Type::Control, "Reset"         },
    {Type::Control, "Service Button"},
    {Type::Control, "Coin 1"        },
    {Type::Control, "Coin 2"        }
  };

  return {};
}

auto PlayChoice10Interface::load() -> bool {
  return system.load(this, System::Model::PlayChoice10);
}

auto PlayChoice10Interface::connect(uint port, uint device) -> void {
  if(port == ID::Port::Expansion) expansionPort.connect(option.port.expansion.device(device));
}
