struct Properties : Setting<string> {
  struct APU : Setting<> { using Setting::Setting;
    Setting<string> ntscVersion{this, "ntscVersion", "RP2A03G"};
    Setting<string> palVersion{this, "palVersion", "RP2A07G"};
    Setting<string> dendyVersion{this, "dendyVersion", "UA6527P"};
  } apu{this, "apu"};

  struct PPU : Setting<> { using Setting::Setting;
    Setting<string> ntscVersion{this, "ntscVersion", "RP2C02G"};
    Setting<string> palVersion{this, "palVersion", "RP2C07"};
    Setting<string> dendyVersion{this, "dendyVersion", "UA6538"};
  } ppu{this, "ppu"};

  struct PC10 : Setting<> { using Setting::Setting;
    Setting<natural> screens{this, "screens", 2};
  } pc10{this, "pc10"};

  Properties() : Setting{"system"} {
    apu.ntscVersion.setValid({
      //NTSC
      "RP2A03",
      "RP2A03A",
      "RP2A03C",
      "RP2A03E",
      "RP2A03F",
      "RP2A03G",
      "RP2A03H",

      //Dendy
      "TA-03NP1-6527P",
      "UA6527P"
    });
    apu.palVersion.setValid({
      "RP2A07G"
    });
    apu.dendyVersion.setValid({
      //Dendy
      "TA-03NP1-6527P",
      "UA6527P",

      //NTSC
      "RP2A03G"
    });
    ppu.ntscVersion.setValid({
      //YIQ
      "RP2C02C",
      "RP2C02E",
      "RP2C02G",

      //RGB
      "RP2C03B",
      "RP2C03G",
      "RC2C03B",
      "RC2C03C"
    });
    ppu.palVersion.setValid({
      "RP2C07"
    });
    ppu.dendyVersion.setValid({
      "UA6538"
    });
    pc10.screens.setValid({1, 2});
  }
};

extern Properties property;
