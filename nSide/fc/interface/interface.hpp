#if defined(CORE_FC)

namespace higan::Famicom {

struct ID {
  enum : uint {
    System,
    Famicom,
    VSSystem,
    PlayChoice10,
    FamicomBox,
  };

  struct Port { enum : uint {
    Hardware,
    Controller1,
    Controller2,
    Expansion,
  };};

  struct Device { enum : uint {
    None,
    FamicomControls,
    VSSystemControls,
    PlayChoice10Controls,
    FamicomBoxControls,

    Gamepad,
    GamepadMic,
    FourScore1,
    FourScore2,
    Zapper,
    PowerPad,
    Vaus,
    SNESGamepad,
    Mouse,

    GamepadE,
    JoyPair,
    FourPlayers,
    BeamGun,
    FamilyKeyboard,
    FamilyTrainer,
    VausE,
    SFCGamepad,
    MouseE,
  };};
};

struct AbstractInterface : Interface {
  auto loaded() -> bool override;
  auto hashes() -> vector<string> override;
  auto manifests() -> vector<string> override;
  auto titles() -> vector<string> override;
  auto save() -> void override;
  auto unload() -> void override;

  auto connected(uint port) -> uint override;
  auto power() -> void override;
  auto reset() -> void override;
  auto run() -> void override;

  auto serialize() -> serializer override;
  auto unserialize(serializer&) -> bool override;

  auto cheats(const vector<string>&) -> void override;

  auto options() -> Settings& override;
  auto properties() -> Settings& override;
};

struct FamicomInterface : AbstractInterface {
  auto information() -> Information;

  auto display() -> Display override;
  auto color(uint32 color) -> uint64 override;

  auto ports() -> vector<Port> override;
  auto devices(uint port) -> vector<Device> override;
  auto inputs(uint device) -> vector<Input> override;

  auto load() -> bool override;

  auto connect(uint port, uint device) -> void override;
};

struct VSSystemInterface : AbstractInterface {
  auto information() -> Information;

  auto display() -> Display override;
  auto color(uint32 color) -> uint64 override;

  auto ports() -> vector<Port> override;
  auto devices(uint port) -> vector<Device> override;
  auto inputs(uint device) -> vector<Input> override;

  auto load() -> bool override;

  auto connect(uint port, uint device) -> void override {}
};

struct PlayChoice10Interface : AbstractInterface {
  auto information() -> Information;

  auto display() -> Display override;
  auto color(uint32 color) -> uint64 override;

  auto ports() -> vector<Port> override;
  auto devices(uint port) -> vector<Device> override;
  auto inputs(uint device) -> vector<Input> override;

  auto load() -> bool override;

  auto connect(uint port, uint device) -> void override;
};

struct FamicomBoxInterface : AbstractInterface {
  auto information() -> Information;

  auto display() -> Display override;
  auto color(uint32 color) -> uint64 override;

  auto ports() -> vector<Port> override;
  auto devices(uint port) -> vector<Device> override;
  auto inputs(uint device) -> vector<Input> override;

  auto load() -> bool override;

  auto connect(uint port, uint device) -> void override;
};

#include "options.hpp"
#include "properties.hpp"

}

#endif
