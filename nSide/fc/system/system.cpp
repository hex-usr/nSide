#include <fc/fc.hpp>

namespace higan::Famicom {

System system;
Scheduler scheduler;
Random random;
Cheat cheat;
#include "serialization.cpp"

auto System::run() -> void {
  if(scheduler.enter() == Scheduler::Event::Frame) {
    if(Famicom::Model::FamicomBox()) famicombox.pollInputs();
    if(!Famicom::Model::VSSystem() || vssystem.gameCount == 2) ppuM.refresh();
    if(Famicom::Model::VSSystem()) ppuS.refresh();
    if(Famicom::Model::PlayChoice10()) playchoice10.videoCircuit.refresh();
    video.refreshFinal();
  }
}

auto System::runToSave() -> void {
  if(!Famicom::Model::VSSystem() || vssystem.gameCount == 2) {
    scheduler.synchronize(cpuM);
    scheduler.synchronize(apuM);
    scheduler.synchronize(ppuM);
  }

  if(Famicom::Model::VSSystem()) {
    scheduler.synchronize(cpuS);
    scheduler.synchronize(apuS);
    scheduler.synchronize(ppuS);
  }

  if(!Famicom::Model::VSSystem() || vssystem.gameCount == 2) {
    scheduler.synchronize(cartridgeSlot[busM.slot]);
    for(auto coprocessor : cpuM.coprocessors) scheduler.synchronize(*coprocessor);
    for(auto peripheral : cpuM.peripherals) scheduler.synchronize(*peripheral);
  }

  if(Famicom::Model::VSSystem()) {
    scheduler.synchronize(cartridgeSlot[busS.slot]);
    for(auto coprocessor : cpuS.coprocessors) scheduler.synchronize(*coprocessor);
    for(auto peripheral : cpuS.peripherals) scheduler.synchronize(*peripheral);
  }
}

auto System::load(Interface* interface, Model model) -> bool {
  information = {};
  information.model = model;

  busM.reset();
  busS.reset();
  cartridgeSlot.append(Cartridge(0));
  if(Famicom::Model::VSSystem()) cartridgeSlot.append(Cartridge(1));
  if(!cartridgeSlot[0].load()) return false;

  if(Famicom::Model::PlayChoice10()) for(uint slot : range(1, 10)) {
    auto cartridge = Cartridge(slot);
    if(!cartridge.load()) break;
    cartridgeSlot.append(cartridge);
  }
  if(Famicom::Model::FamicomBox()) for(uint slot : range(1, 15)) {
    auto cartridge = Cartridge(slot);
    if(!cartridge.load()) break;
    cartridgeSlot.append(cartridge);
  }

  if(Famicom::Model::Famicom()) {
    if(cartridgeSlot[0].region() == "NTSC-J") {
      information.region = Region::NTSCJ;
      information.frequency = Constants::Colorburst::NTSC * 6.0;
    }
    if(cartridgeSlot[0].region() == "NTSC-U") {
      information.region = Region::NTSCU;
      information.frequency = Constants::Colorburst::NTSC * 6.0;
    }
    if(cartridgeSlot[0].region() == "PAL") {
      information.region = Region::PAL;
      information.frequency = Constants::Colorburst::PAL * 6.0;
    }
    if(cartridgeSlot[0].region() == "Dendy") {
      information.region = Region::Dendy;
      information.frequency = Constants::Colorburst::PAL * 6.0;
    }
  } else {
    information.region = Famicom::Model::PlayChoice10() ? Region::NTSCU : Region::NTSCJ;
    information.frequency = Constants::Colorburst::NTSC * 6.0;
  }

  if(!cpuM.load()) return false;
  if(!apuM.load()) return false;
  if(!ppuM.load()) return false;

  if(Famicom::Model::VSSystem()) {
    if(!cpuS.load()) return false;
    if(!apuS.load()) return false;
    if(!ppuS.load()) return false;
  }

  switch(model) {

  case Model::VSSystem: {
    vssystem.load();
    break;
  }

  case Model::PlayChoice10: {
    if(!playchoice10.load()) return false;
    break;
  }

  case Model::FamicomBox: {
    if(!famicombox.load()) return false;
    break;
  }

  }

  serializeInit();
  this->interface = interface;
  return information.loaded = true;
}

auto System::save() -> void {
  if(!loaded()) return;

  if(!Famicom::Model::VSSystem()) {
    for(auto& cartridge : cartridgeSlot) cartridge.save();
  } else {
    cartridgeSlot[2 - vssystem.gameCount].save();
  }
}

auto System::unload() -> void {
  if(!loaded()) return;

  cpuM.peripherals.reset();
  cpuS.peripherals.reset();
  controllerPortM1.unload();
  controllerPortM2.unload();
  expansionPort.unload();
  controllerPortS1.unload();
  controllerPortS2.unload();

  if(!Famicom::Model::VSSystem()) {
    for(auto& cartridge : cartridgeSlot) cartridge.unload();
  } else {
    cartridgeSlot[2 - vssystem.gameCount].unload();
  }

  if(Famicom::Model::FamicomBox()) famicombox.unload();

  cartridgeSlot.reset();
  information.loaded = false;
}

auto System::power(bool reset) -> void {
  video.reset(interface);
  video.setPalette();
  video.setEffect(Video::Effect::Scanlines, option.video.scanlineEmulation());
  if(model() == Model::Famicom || model() == Model::FamicomBox) {
    video.resize(256, 240);
  } else if(model() == Model::VSSystem) {
    video.resize(256 * vssystem.gameCount, 240);
  } else if(model() == Model::PlayChoice10) {
    video.resize(256, 240 + (property.pc10.screens() - 1) * 224);
  }
  audio.reset(interface);
  random.entropy(Random::Entropy::Low);

  scheduler.reset();

  if(!Famicom::Model::VSSystem() || vssystem.gameCount == 2) {
    cartridgeSlot[busM.slot].power(reset);
    cpuM.power(reset);
    apuM.power(reset);
    ppuM.power(reset);
  }

  if(Famicom::Model::VSSystem()) {
    cartridgeSlot[busS.slot].power(reset);
    cpuS.power(reset);
    apuS.power(reset);
    ppuS.power(reset);
  }

  switch(model()) {
  case Model::VSSystem:     vssystem.power(reset); break;
  case Model::PlayChoice10: playchoice10.power(reset); break;
  case Model::FamicomBox:   famicombox.power(reset); break;
  }

  switch(model()) {
  case Model::VSSystem:     cpuM.coprocessors.append(&vssystem); cpuS.coprocessors.append(&vssystem); break;
  case Model::PlayChoice10: cpuM.coprocessors.append(&playchoice10.pc10cpu); break;
  case Model::FamicomBox:   cpuM.coprocessors.append(&famicombox); break;
  }

  scheduler.primary(Famicom::Model::VSSystem() && vssystem.gameCount == 1 ? cpuS : cpuM);

  if(!Famicom::Model::VSSystem() || vssystem.gameCount == 2) {
    controllerPortM1.power(0, ID::Port::Controller1);
    controllerPortM2.power(0, ID::Port::Controller2);
  }
  if(!Famicom::Model::VSSystem()) expansionPort.power();
  if(Famicom::Model::VSSystem()) {
    controllerPortS1.power(1, ID::Port::Controller1);
    controllerPortS2.power(1, ID::Port::Controller2);
  }

  switch(model()) {

  case Model::Famicom:
  case Model::FamicomBox: {
    controllerPortM1.connect(option.port.controller1.device());
    controllerPortM2.connect(option.port.controller2.device());
    expansionPort.connect(option.port.expansion.device());
    break;
  }

  case Model::VSSystem: {
    auto side = cartridgeSlot[0].game.document.find("side");
    for(bool sideIndex : range(side.size())) {
      bool sideID = sideIndex + (2 - side.size());

      bool& swapControllers = sideID ? vssystem.swapControllersS : vssystem.swapControllersM;
      swapControllers = side(side.size() - 1)["controller/port"].integer() == 2;

      if(side(sideIndex)["ppu"]) {
        string device1 = side(sideIndex).find("controller(port=1)/device")(0).text();
        string device2 = side(sideIndex).find("controller(port=2)/device")(0).text();

        auto& controllerPort1 = sideID ? controllerPortS1 : controllerPortM1;
        auto& controllerPort2 = sideID ? controllerPortS2 : controllerPortM2;

        if(device1 == "gamepad") {
          controllerPort1.connect(ID::Device::Gamepad);
        } else if(device1 == "zapper") {
          controllerPort1.connect(ID::Device::Zapper);
        } else {
          controllerPort1.connect(ID::Device::None);
        }

        if(device2 == "gamepad") {
          controllerPort2.connect(ID::Device::Gamepad);
        } else if(device2 == "zapper") {
          controllerPort2.connect(ID::Device::Zapper);
        } else {
          controllerPort2.connect(ID::Device::None);
        }
      }
    }
    break;
  }

  case Model::PlayChoice10: {
    controllerPortM1.connect(ID::Device::Gamepad);
    controllerPortM2.connect(ID::Device::Gamepad);
    expansionPort.connect(option.port.expansion.device());
    break;
  }

  }
}

}
