#define cpu (apu.side ? cpuS : cpuM)

APU::DMC::DMC(APU& apu) : apu(apu) {
}

#include <higan/fc/apu/dmc.cpp>

#undef cpu
