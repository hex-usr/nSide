auto Cartridge::saveCartridge() -> void {
  if(auto node = boardNode["prg/memory(type=RAM)"]) saveMemory(board->prgram, node);
  if(auto node = boardNode["chr/memory(type=RAM)"]) saveMemory(board->chrram, node);
  if(board->chip) {
    if(auto node = boardNode["chip/memory(type=RAM)"]) saveMemory(board->chip->ram, node);
  }
}

//

auto Cartridge::saveMemory(Memory::Writable<uint8>& ram, Game::Memory memory, maybe<uint> id) -> void {
  if(!id) id = pathID();
  if(memory.type == "RAM" && !memory.nonVolatile) return;
  if(auto fp = platform->open(id(), memory.name(), File::Write)) {
    ram.save(fp);
  }
}

auto Cartridge::saveMemory(Memory::Writable<uint8>& ram, Markup::Node node, maybe<uint> id) -> void {
  if(!id) id = pathID();
  if(auto memory = game.memory(node)) {
    saveMemory(ram, *memory, id);
  } else if(node["name"] && node["size"]) {
    if(node["volatile"]) return;
    if(auto fp = platform->open(id(), node["name"].text(), File::Write)) {
      ram.save(fp);
    }
  }
}
