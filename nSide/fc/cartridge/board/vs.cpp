//VS

struct VS : Board {
  VS(Markup::Node& boardNode, string chipType_) : Board(boardNode),
  mmc1(*this, chipType_),
  n108(*this, boardNode) {
    chipType = ChipType::None;
    if(chipType_.match("74HC32")) chipType = ChipType::_74HC32;
    if(chipType_.match("MMC1*" )) chipType = ChipType::MMC1;
    if(chipType_.match("108"   )) chipType = ChipType::N108;
    if(chipType_.match("109"   )) chipType = ChipType::N108;
    if(chipType_.match("118"   )) chipType = ChipType::N108;
    if(chipType_.match("119"   )) chipType = ChipType::N108;
  }

  auto main() -> void {
    if(chipType == ChipType::MMC1) return mmc1.main();
    tick();
  }

  auto readPRG(uint addr, uint8 data) -> uint8 {
    switch(chipType) {
    case ChipType::None: {
      if(addr & 0x8000) {
        if(addr < 0xe000 && prgrom.size() < 0x2000) return data;
        if(addr < 0xc000 && prgrom.size() < 0x4000) return data;
        if(addr < 0xa000 && prgrom.size() < 0x6000) return data;
        addr &= 0x7fff;
        if(prgrom.size() > 0x8000) {  //Games with oversize 1D such as VS. Gumshoe
          if(addr >= 0x2000 || bank == 1) addr += 0x2000;
        }
        return prgrom.read(addr);
      }
      break;
    }

    case ChipType::_74HC32: {
      if(addr & 0x8000) {
        if((addr & 0xc000) == 0x8000)
          return prgrom.read((bank << 14) | (addr & 0x3fff));
        else
          return prgrom.read((0x0f << 14) | (addr & 0x3fff));
      }
      break;
    }

    case ChipType::MMC1: {
      if(addr & 0x8000) return prgrom.read(mmc1.prgAddress(addr));
      //if(revision == Revision::SUROM || revision == Revision::SXROM) {
      //  addr |= ((mmc1.chrBank[lastCHRBank] & 0x10) >> 4) << 18;
      //}
      break;
    }

    case ChipType::N108: {
      if(addr & 0x8000) return prgrom.read(n108.prgAddress(addr));
      break;
    }

    }
    if((addr & 0xe000) == 0x6000 && prgram.size() > 0) return prgram.read(addr);
    return data;
  }

  auto writePRG(uint addr, uint8 data) -> void {
    switch(chipType) {
    case ChipType::None:
      if(addr == 0x4016) bank = data.bit(2);
      break;
    case ChipType::_74HC32:
      //TODO: Check if VS. UNROM has bus conflicts
      //data &= readPRG(addr, data);
      if(addr & 0x8000) bank = data.bits(0,3);
      break;
    case ChipType::MMC1:
      if(addr & 0x8000) return mmc1.mmioWrite(addr, data);
      break;
    case ChipType::N108:
      if(addr & 0x8000) return n108.regWrite(addr, data);
      break;
    }
    if((addr & 0xe000) == 0x6000 && prgram.size() > 0) return prgram.write(addr, data);
  }

  auto readCHR(uint addr, uint8 data) -> uint8 {
    if(addr & 0x2000) return ppu.ciram.read(addr);
    switch(chipType) {
    case ChipType::_74HC32: return Board::readCHR(addr, data);
    case ChipType::MMC1:    return Board::readCHR(mmc1.chrAddress(addr), data);
    case ChipType::N108:    return Board::readCHR(n108.chrAddress(addr), data);
    }
    if(chrrom.size() < bank << 13) return data;
    return chrrom.read(((bank << 13) + (addr & 0x1fff)));
  }

  auto writeCHR(uint addr, uint8 data) -> void {
    if(addr & 0x2000) return ppu.ciram.write(addr, data);
    switch(chipType) {
    case ChipType::_74HC32: return Board::writeCHR(addr, data);
    case ChipType::MMC1:    return Board::writeCHR(mmc1.chrAddress(addr), data);
    case ChipType::N108:    return Board::writeCHR(n108.chrAddress(addr), data);
    }
  }

  auto power(bool reset) -> void {
    bank = 0;
    switch(chipType) {
    case ChipType::MMC1: mmc1.power(reset); break;
    case ChipType::N108: n108.power(reset); break;
    }
  }

  auto serialize(serializer& s) -> void {
    Board::serialize(s);
    s.integer(bank);
    switch(chipType) {
    case ChipType::MMC1: mmc1.serialize(s); break;
    case ChipType::N108: n108.serialize(s); break;
    }
  }

  enum class ChipType : uint {
    None,
    _74HC32,
    MMC1,
    N108,
  } chipType;

  uint4 bank;
  MMC1 mmc1;
  N108 n108;
};
