struct Board {
  inline auto rate() const -> uint { return Region::PAL() ? 16 : Region::Dendy() ? 15 : 12; }

  Board(Markup::Node& boardNode, string chipType = "");
  virtual ~Board();

  virtual auto main() -> void;
  virtual auto tick() -> void;

  virtual uint8 readPRG(uint addr, uint8 data) = 0;
  virtual void writePRG(uint addr, uint8 data) = 0;

  virtual auto readCHR(uint addr, uint8 data) -> uint8;
  virtual auto writeCHR(uint addr, uint8 data) -> void;

  virtual inline auto scanline(uint y) -> void {}

  virtual auto power(bool reset) -> void;

  virtual auto serialize(serializer&) -> void;

  static Board* load(Markup::Node boardNode, string chipType);

  uint slot;

  Chip* chip = nullptr;

  Memory::Readable<uint8> prgrom;
  Memory::Writable<uint8> prgram;
  Memory::Readable<uint8> chrrom;
  Memory::Writable<uint8> chrram;
  Memory::Readable<uint8> instrom;
  Memory::Readable<uint8> keyrom;
};
