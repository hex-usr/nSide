struct Board;

struct Chip {
  Chip(Board& board);
  auto tick() -> void;

  Board& board;
  Memory::Writable<uint8> ram;
};
