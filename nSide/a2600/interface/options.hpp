struct Options : Setting<> {
  struct Port : Setting<> { using Setting::Setting;
    struct Controller1 : Setting<> { using Setting::Setting;
      Setting<natural> device{this, "device", ID::Device::Joystick};
    } controller1{this, "controller1"};

    struct Controller2 : Setting<> { using Setting::Setting;
      Setting<natural> device{this, "device", ID::Device::Joystick};
    } controller2{this, "controller2"};
  } port{this, "port"};

  Setting<boolean> random{this, "entropy", true};

  struct Video : Setting<> { using Setting::Setting;
    Setting<boolean> colorEmulation{this, "colorEmulation", true};
    Setting<boolean> scanlineEmulation{this, "scanlineEmulation", true};
  } video{this, "video"};

  Options() : Setting{"options"} {
    video.colorEmulation.onModify([&] {
      higan::video.setPalette();
    });
    video.scanlineEmulation.onModify([&] {
      higan::video.setEffect(higan::Video::Effect::Scanlines, video.scanlineEmulation());
    });
  }
};

extern Options option;
