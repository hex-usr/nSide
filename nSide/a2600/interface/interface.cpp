#include <a2600/a2600.hpp>

namespace higan::Atari2600 {

Options option;
Properties property;

auto Atari2600Interface::information() -> Information {
  Information information;
  information.manufacturer = "Atari";
  information.name         = "Atari 2600";
  information.extension    = "a26";
  return information;
}

auto Atari2600Interface::display() -> Display {
  double squarePixelRate = Atari2600::Region::NTSC()
  ? 135.0 / 22.0 * 1'000'000.0
  : 7'375'000.0;

  Display display;
  display.type   = Display::Type::CRT;
  display.colors = 1 << 7;
  display.width  = 160;
  display.height = 228;
  display.internalWidth  = 160;
  display.internalHeight = 228;
  display.aspectCorrection = squarePixelRate / system.frequency();
  return display;
}

auto Atari2600Interface::color(uint32 color) -> uint64 {
  static auto generateNTSCColor = [](uint7 n, double hue, double gamma) -> uint64 {
    uint4 color = n.bits(3,6);
    uint3 level = n.bits(0,2);

    double y;
    double i;
    double q;

    //TODO: Determine if there is any special circuitry for when both the
    //luminosity and hue are 0 (black).
    if(color == 0 && level == 0) y = 0.0;
    else y = 0.125 + level / 7.0 * 0.875;

    if(color == 0) {
      i = 0.0;
      q = 0.0;
    } else {
      //hue 15 == hue 1:                   (360.0 / 14.0)°
      //hue 15 == (hue 1 + hue 2) / 2:     (360.0 / (14.0 - 1.0 / 2.0))°
      //hue 15 == (hue 1 + hue 2 * 2) / 3: (360.0 / (14.0 - 2.0 / 3.0))°
      static const double delay = (360.0 / (14.0 - 2.0 / 3.0)) * Math::Pi / 180.0;
      //phase shift delay only applies to colors 2-15
      double phase = Math::Pi + hue - (color - 1) * delay;
      i = std::sin(phase - 33.0 * Math::Pi / 180.0) * 0.25;
      q = std::cos(phase - 33.0 * Math::Pi / 180.0) * 0.25;
    }

    auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
    //This matrix is from FCC's 1953 NTSC standard.
    //The Atari 2600 is older than the SMPTE C standard that followed in 1987.
    uint64 r = uclamp<16>(65535.0 * gammaAdjust(y +  0.946882 * i +  0.623557 * q));
    uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.274788 * i + -0.635691 * q));
    uint64 b = uclamp<16>(65535.0 * gammaAdjust(y + -1.108545 * i +  1.709007 * q));

    return r << 32 | g << 16 | b << 0;
  };

  static auto generatePALColor = [](uint7 n, double gamma) -> uint64 {
    uint4 color = n.bits(3,6);
    uint3 level = n.bits(0,2);

    double y;
    double u;
    double v;

    //TODO: Determine the real formula for generating colors. The below formula
    //is a quick hack-up to match colors with publicly-available palettes.
    if((color.bits(1,3) == 0 || color.bits(1,3) == 7) && level == 0) y = 0.0;
    else y = 0.125 + level / 7.0 * 0.875;

    if(color.bits(1,3) == 0 || color.bits(1,3) == 7) {
      u = 0.0;
      v = 0.0;
    } else if(color.bit(0) == 0) {
      double phase = (180.0 - (color >> 1) * 30.0) * Math::Pi / 180.0;
      u = std::cos(phase) * 0.25;
      v = std::sin(phase) * 0.25;
    } else if(color.bit(0) == 1) {
      double phase = (165.0 + (color >> 1) * 30.0) * Math::Pi / 180.0;
      u = std::cos(phase) * 0.25;
      v = std::sin(phase) * 0.25;
    }

    auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
    uint64 r = uclamp<16>(65535.0 * gammaAdjust(y                 +  1.139837 * v));
    uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.394652 * u + -0.580599 * v));
    uint64 b = uclamp<16>(65535.0 * gammaAdjust(y +  2.032110 * u                ));

    return r << 32 | g << 16 | b << 0;
  };

  static auto generateSECAMColor = [](uint7 n, double gamma) -> uint64 {
    uint3 level = n.bits(0,2);

    //static const uint32 colors[] = {
    //  0xff000000, 0xff2121ff, 0xfff03c79, 0xffff50ff,
    //  0xff7fff50, 0xff7fffff, 0xffffff3f, 0xffffffff,
    //};
    static const double Y[] = {
       0.0000000000000000000,  0.2286588235294117800,
       0.4736235294117646700,  0.5971568627450980000,
       0.7716784313725490000,  0.8499137254901961000,
       0.9141647058823529000,  1.0000000000000000000,
    };
    static const double Db[] = {
       0.0000000000000000000,  1.1604941176470587000,
       0.0012274509803921196,  0.6059803921568627000,
      -0.6889215686274510000,  0.2258823529411764200,
      -1.0036705882352940000,  0.0000000000000000000,
    };
    static const double Dr[] = {
       0.0000000000000000000,  0.1889176470588235500,
      -0.8890313725490195000, -0.7658823529411765000,
       0.5201921568627452000,  0.6691137254901962000,
      -0.1633882352941175000,  0.0000000000000000000,
    };

    double y  = Y[level];
    double db = Db[level];
    double dr = Dr[level];

    //uint64 r = image::normalize(colors[level].byte(2), 8, 16);
    //uint64 g = image::normalize(colors[level].byte(1), 8, 16);
    //uint64 b = image::normalize(colors[level].byte(0), 8, 16);
    auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
    uint64 r = uclamp<16>(65535.0 * gammaAdjust(y +  0.000092303716148 * db + -0.525912630661865 * dr));
    uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.129132898890509 * db +  0.267899328207599 * dr));
    uint64 b = uclamp<16>(65535.0 * gammaAdjust(y +  0.664679059978955 * db + -0.000079202543533 * dr));

    return r << 32 | g << 16 | b << 0;
  };

  double gamma = option.video.colorEmulation() ? 1.8 : 2.2;
  if(system.region() == System::Region::NTSC) {
    return generateNTSCColor(color, 0.0, gamma);
  } else if(system.region() == System::Region::PAL) {
    return generatePALColor(color, gamma);
  } else if(system.region() == System::Region::SECAM) {
    return generateSECAMColor(color, gamma);
  }
  unreachable;
}

auto Atari2600Interface::loaded() -> bool {
  return system.loaded();
}

auto Atari2600Interface::hashes() -> vector<string> {
  return {cartridge.sha256()};
}

auto Atari2600Interface::manifests() -> vector<string> {
  return {cartridge.manifest()};
}

auto Atari2600Interface::titles() -> vector<string> {
  return {cartridge.title()};
}

auto Atari2600Interface::load() -> bool {
  return system.load(this);
}

auto Atari2600Interface::save() -> void {
  system.save();
}

auto Atari2600Interface::unload() -> void {
  save();
  system.unload();
}

auto Atari2600Interface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Hardware,    "Hardware"         }};
}

auto Atari2600Interface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::None,     "None"    },
    {ID::Device::Joystick, "Joystick"}
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::None,     "None"    },
    {ID::Device::Joystick, "Joystick"}
  };

  if(port == ID::Port::Hardware) return {
    {ID::Device::Controls, "Controls"}
  };

  return {};
}

auto Atari2600Interface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::Joystick) return {
    {Type::Hat,     "Up"   },
    {Type::Hat,     "Down" },
    {Type::Hat,     "Left" },
    {Type::Hat,     "Right"},
    {Type::Button,  "Fire" }
  };

  if(device == ID::Device::Controls) return {
    {Type::Control, "Select"          },
    {Type::Control, "Reset"           },
    {Type::Control, "Color"           },
    {Type::Control, "Left Difficulty" },
    {Type::Control, "Right Difficulty"}
  };

  return {};
}

auto Atari2600Interface::connected(uint port) -> uint {
  if(port == ID::Port::Controller1) return option.port.controller1.device();
  if(port == ID::Port::Controller2) return option.port.controller2.device();
  return 0;
}

auto Atari2600Interface::connect(uint port, uint device) -> void {
  if(port == ID::Port::Controller1) controllerPort1.connect(option.port.controller1.device(device));
  if(port == ID::Port::Controller2) controllerPort2.connect(option.port.controller2.device(device));
}

auto Atari2600Interface::power() -> void {
  system.power();
}

auto Atari2600Interface::run() -> void {
  system.run();
}

auto Atari2600Interface::serialize() -> serializer {
  system.runToSave();
  return system.serialize();
}

auto Atari2600Interface::unserialize(serializer& s) -> bool {
  return system.unserialize(s);
}

auto Atari2600Interface::cheats(const vector<string>& list) -> void {
  cheat.reset();
  cheat.assign(list);
}

auto Atari2600Interface::options() -> Settings& {
  return option;
}

auto Atari2600Interface::properties() -> Settings& {
  return property;
}

}
