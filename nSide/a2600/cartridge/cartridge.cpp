#include <a2600/a2600.hpp>

namespace higan::Atari2600 {

#include "serialization.cpp"
Cartridge cartridge;

auto Cartridge::load() -> bool {
  information = {};

  if(auto loaded = platform->load(ID::Atari2600, "Atari 2600", "a26", {"Auto", "NTSC", "PAL", "SECAM"})) {
    information.pathID = loaded.pathID;
    information.region = loaded.option;
  } else return false;

  if(auto fp = platform->open(pathID(), "manifest.bml", File::Read, File::Required)) {
    information.manifest = fp->reads();
  } else return false;

  auto document = BML::unserialize(information.manifest);
  information.title = document["game/label"].text();

  if(information.region == "Auto") {
    if(auto region = document["game/region"].text()) {
      information.region = region.upcase();
    } else {
      information.region = "NTSC";
    }
  }

  if(auto memory = Game::Memory{document["game/board/memory(type=ROM,content=Program)"]}) {
    rom.allocate(memory.size);
    if(auto fp = platform->open(pathID(), memory.name(), File::Read, File::Required)) {
      rom.load(fp);
    }
  }

  if(auto memory = Game::Memory{document["game/board/memory(type=RAM,content=Save)"]}) {
    ram.allocate(memory.size);
    if(memory.nonVolatile) {
      if(auto fp = platform->open(pathID(), memory.name(), File::Read)) {
        ram.load(fp);
      }
    }
  }

  information.sha256 = Hash::SHA256({rom.data(), rom.size()}).digest();
  return true;
}

auto Cartridge::save() -> void {
  auto document = BML::unserialize(information.manifest);

  if(auto memory = Game::Memory{document["game/board/memory(type=RAM,content=Save)"]}) {
    if(memory.nonVolatile) {
      if(auto fp = platform->open(pathID(), memory.name(), File::Write)) {
        ram.save(fp);
      }
    }
  }
}

auto Cartridge::unload() -> void {
  rom.reset();
  ram.reset();
}

auto Cartridge::power() -> void {
}

auto Cartridge::reset() -> void {
}

auto Cartridge::access(uint13 address, uint8 data) -> uint8 {
  if(!address.bit(12)) return data;

  if(ram) {
    if((address & rom.mask()) < ram.size() << 0) return ram.write(address, data), data;
    if((address & rom.mask()) < ram.size() << 1) return ram.read(address);
  }

  return rom[address];
}

}
