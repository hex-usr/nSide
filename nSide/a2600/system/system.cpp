#include <a2600/a2600.hpp>

namespace higan::Atari2600 {

System system;
Scheduler scheduler;
Random random;
Cheat cheat;
#include "serialization.cpp"

auto System::run() -> void {
  if(scheduler.enter() == Scheduler::Event::Frame) tia.refresh();
}

auto System::runToSave() -> void {
  scheduler.synchronize(cpu);
  scheduler.synchronize(pia);
  scheduler.synchronize(tia);
  for(auto peripheral : pia.peripherals) scheduler.synchronize(*peripheral);
}

auto System::load(Interface* interface) -> bool {
  this->interface = interface;
  information = {};

  if(!cartridge.load()) return false;

  if(cartridge.region() == "NTSC") {
    information.region = Region::NTSC;
    information.frequency = Constants::Colorburst::NTSC;
  }
  if(cartridge.region() == "PAL") {
    information.region = Region::PAL;
    information.frequency = Constants::Colorburst::PAL * 4.0 / 5.0;
  }
  if(cartridge.region() == "SECAM") {
    information.region = Region::SECAM;
    information.frequency = Constants::Colorburst::PAL * 4.0 / 5.0;
  }

  serializeInit();
  return information.loaded = true;
}

auto System::save() -> void {
  if(!loaded()) return;

  cartridge.save();
}

auto System::unload() -> void {
  if(!loaded()) return;

  pia.peripherals.reset();
  controllerPort1.unload();
  controllerPort2.unload();

  cartridge.unload();
  information.loaded = false;
}

auto System::power() -> void {
  video.reset(interface);
  video.setPalette();
  video.setEffect(Video::Effect::Scanlines, option.video.scanlineEmulation());
  audio.reset(interface);
  random.entropy(Random::Entropy::Low);

  scheduler.reset();
  cpu.power();
  pia.power();
  tia.power();
  scheduler.primary(cpu);

  controllerPort1.power(ID::Port::Controller1);
  controllerPort2.power(ID::Port::Controller2);

  controllerPort1.connect(option.port.controller1.device());
  controllerPort2.connect(option.port.controller2.device());
}

}
