auto SG1000Interface::information() -> Information {
  Information information;
  information.manufacturer = "Sega";
  information.name         = "SG-1000";
  information.extension    = "sg1000";
  return information;
}

auto SG1000Interface::display() -> Display {
  double squarePixelRate = system.region() == System::Region::NTSC
  ? 135.0 / 22.0 * 1'000'000.0
  : 7'375'000.0;

  Display display;
  display.type   = Display::Type::CRT;
  display.colors = 1 << 4;
  display.width  = 256;
  display.height = 192;
  display.internalWidth  = 256;
  display.internalHeight = 192;
  display.aspectCorrection = squarePixelRate / (system.colorburst() * 15.0 / 10.0);
  return display;
}

auto SG1000Interface::color(uint32 color) -> uint64 {
  double gamma = option.video.colorEmulation() ? 1.8 : 2.2;

  static double Y[] = {
    0.00, 0.00, 0.53, 0.67,
    0.40, 0.53, 0.47, 0.67,
    0.53, 0.67, 0.73, 0.80,
    0.46, 0.53, 0.80, 1.00,
  };
  static double Saturation[] = {
    0.000, 0.000, 0.267, 0.200,
    0.300, 0.267, 0.233, 0.300,
    0.300, 0.300, 0.233, 0.167,
    0.233, 0.200, 0.000, 0.000,
  };
  static uint Phase[] = {
      0,   0, 237, 235,
    354, 354, 114, 295,
    114, 114, 173, 173,
    235,  53,   0,   0,
  };
  double y = Y[color];
  double i = Saturation[color] * std::sin((Phase[color] - 33) * Math::Pi / 180.0);
  double q = Saturation[color] * std::cos((Phase[color] - 33) * Math::Pi / 180.0);

  auto gammaAdjust = [=](double f) -> double { return f < 0.0 ? 0.0 : std::pow(f, 2.2 / gamma); };
  //This matrix is from FCC's 1953 NTSC standard.
  //The SG-1000, ColecoVision, and MSX are older than the SMPTE C standard that followed in 1987.
  uint64 r = uclamp<16>(65535.0 * gammaAdjust(y +  0.946882 * i +  0.623557 * q));
  uint64 g = uclamp<16>(65535.0 * gammaAdjust(y + -0.274788 * i + -0.635691 * q));
  uint64 b = uclamp<16>(65535.0 * gammaAdjust(y + -1.108545 * i +  1.709007 * q));

  return r << 32 | g << 16 | b << 0;
}

auto SG1000Interface::ports() -> vector<Port> { return {
  {ID::Port::Controller1, "Controller Port 1"},
  {ID::Port::Controller2, "Controller Port 2"},
  {ID::Port::Hardware,    "Hardware"         }};
}

auto SG1000Interface::devices(uint port) -> vector<Device> {
  if(port == ID::Port::Controller1) return {
    {ID::Device::None,    "None"   },
    {ID::Device::Gamepad, "Gamepad"}
  };

  if(port == ID::Port::Controller2) return {
    {ID::Device::None,    "None"   },
    {ID::Device::Gamepad, "Gamepad"}
  };

  if(port == ID::Port::Hardware) return {
    {ID::Device::SG1000Controls, "Controls"}
  };

  return {};
}

auto SG1000Interface::inputs(uint device) -> vector<Input> {
  using Type = Input::Type;

  if(device == ID::Device::None) return {
  };

  if(device == ID::Device::Gamepad) return {
    {Type::Hat,    "Up"   },
    {Type::Hat,    "Down" },
    {Type::Hat,    "Left" },
    {Type::Hat,    "Right"},
    {Type::Button, "1"    },
    {Type::Button, "2"    }
  };

  if(device == ID::Device::SG1000Controls) return {
    {Type::Control, "Pause"}
  };

  return {};
}

auto SG1000Interface::load() -> bool {
  return system.load(this, System::Model::SG1000);
}

auto SG1000Interface::connected(uint port) -> uint {
  if(port == ID::Port::Controller1) return option.port.controller1.device();
  if(port == ID::Port::Controller2) return option.port.controller2.device();
  if(port == ID::Port::Hardware) return ID::Device::SG1000Controls;
  return 0;
}

auto SG1000Interface::connect(uint port, uint device) -> void {
  if(port == ID::Port::Controller1) controllerPort1.connect(option.port.controller1.device(device));
  if(port == ID::Port::Controller2) controllerPort2.connect(option.port.controller2.device(device));
}
