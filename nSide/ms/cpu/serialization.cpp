auto CPU::serialize(serializer& s) -> void {
  Z80::serialize(s);
  Z80::Bus::serialize(s);
  Thread::serialize(s);

  s.array(ram.data(), ram.size());

  s.integer(state.nmiLine);
  s.integer(state.intLine);

  if(Model::ColecoVision()) {
    s.integer(coleco.replaceBIOS);
    s.integer(coleco.replaceRAM);
  } else {
    s.integer(sega.disableIO);
    s.integer(sega.disableBIOS);
    s.integer(sega.disableRAM);
    s.integer(sega.disableMyCard);
    s.integer(sega.disableCartridge);
    s.integer(sega.disableExpansion);
  }
}
