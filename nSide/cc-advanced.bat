@echo off
rem For use with Windows

echo Release targets:
echo   "star-rod"  fork of laevateinn, the debugger included with bsnes v086
echo WIP and development targets:
echo   "prealpha"  enable support for unfinished emulators: A2600, MSX, NGP
echo   "shimai"    raster UI inspired by the NES Classic Edition
echo   "console"   show console window during emulation
set /p target=Target:

if /i "%target%" equ "prealpha" (
  mingw32-make -j6 console=true cores="fc ms md pce gb gba ws a2600 msx ngp"
  if not exist "out\nSide.exe" (pause)
)
if /i "%target%" equ "shimai" (
  mingw32-make -j6 target=shimai
  if not exist "out\shimai.exe" (pause)
)
if /i "%target%" equ "console" (
  mingw32-make -j6 console=true
  if not exist "out\nSide.exe" (pause)
)
if /i "%target%" equ "debug" (
  mingw32-make -j6 build=debug console=true cores="fc ms md pce gb gba ws a2600 msx ngp"
  if not exist "out\nSide.exe" (pause)
)

@echo on