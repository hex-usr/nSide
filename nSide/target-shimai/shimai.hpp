//shimai
//From the term 「四枚の葉」, which means "four-leaf clover", named after the
//NES Classic Edition's code name "Clover".

#include <nall/nall.hpp>
using namespace nall;

#include <ruby/ruby.hpp>
using namespace ruby;
extern unique_pointer<Video> video;
extern unique_pointer<Audio> audio;
extern unique_pointer<Input> input;

#include <hiro/hiro.hpp>
using namespace hiro;

#include <emulator/emulator.hpp>
extern higan::Interface* emulator;

#include "program/program.hpp"
#include "input/input.hpp"
#include "graphics/graphics.hpp"
#include "sound/sound.hpp"
#include "controls/controls.hpp"
#include "theme/theme.hpp"
#include "scene/scene.hpp"
#include "home/home.hpp"
#include "settings/settings.hpp"
#include "presentation/presentation.hpp"

auto locate(string name) -> string;
auto locateSystem(string name) -> string;
