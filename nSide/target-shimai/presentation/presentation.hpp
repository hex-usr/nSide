struct Presentation : Window {
  Presentation();
  auto updateEmulator() -> void;
  auto configureViewport() -> void;
  auto clearViewport() -> void;
  auto resizeViewport() -> void;
  auto resizeWindow() -> void;
  auto toggleFullScreen() -> void;
  auto drawMenu() -> void;

  VerticalLayout layout{this};
    HorizontalLayout viewportLayout{&layout, Size{~0, ~0}, 0};
      Viewport viewport{&viewportLayout, Size{~0, ~0}, 0};
};

extern unique_pointer<Presentation> presentation;
