#include "../shimai.hpp"
unique_pointer<Presentation> presentation;

Presentation::Presentation() {
  presentation = this;

  viewport.setDroppable().onDrop([&](vector<string> locations) {
    if(!locations || !directory::exists(locations.first())) return;
    program->gameQueue.append(locations.first());
    program->load();
  }).onSize([&] {
    configureViewport();
  });

  onClose([&] {
    program->quit();
  });

  setTitle({"shimai v", nSide::Version});
  setResizable(false);
  setBackgroundColor({0, 0, 0});
  resizeWindow();
  setCentered();

  #if defined(PLATFORM_MACOS)
  Application::Cocoa::onActivate([&] { setFocused(); });
  Application::Cocoa::onQuit([&] { doClose(); });
  #endif
}

auto Presentation::updateEmulator() -> void {
  if(!emulator) return;

  emulator->setOption("video/colorBleed", settings["Video/BlurEmulation"].boolean());
  emulator->setOption("video/interframeBlending", settings["Video/BlurEmulation"].boolean());
  emulator->setOption("video/colorEmulation", settings["Video/ColorEmulation"].boolean());
  emulator->setOption("video/scanlineEmulation", settings["Video/ScanlineEmulation"].boolean());
}

auto Presentation::configureViewport() -> void {
  uint width = viewport.geometry().width();
  uint height = viewport.geometry().height();
  if(video) video->configure(width, height, 60, 60);
}

auto Presentation::clearViewport() -> void {
  if(!emulator || !emulator->loaded()) viewportLayout.setPadding();
  if(!visible() || !video) return;

  uint width = 16;
  uint height = 16;
  if(auto [output, length] = video->acquire(width, height); length) {
    for(uint y : range(height)) {
      auto line = output + y * (length >> 2);
      for(uint x : range(width)) *line++ = 0xff000000;
    }
    video->release();
    video->output();
  }
}

auto Presentation::resizeViewport() -> void {
  uint layoutWidth = viewportLayout.geometry().width();
  uint layoutHeight = viewportLayout.geometry().height();

  double width = 1280.0 / 3.0;
  uint height = 240;

  if(emulator) {
    auto display = emulator->display();
    width = display.width;
    height = display.height;
    if(settings["View/AspectCorrection"].boolean()) width *= display.aspectCorrection;
    if(!settings["View/Overscan"].boolean()) {
      if(display.type == higan::Interface::Display::Type::CRT) {
        uint overscanHorizontal = settings["View/Overscan/Horizontal"].natural();
        uint overscanVertical = settings["View/Overscan/Vertical"].natural();
        width -= overscanHorizontal * 2;
        height -= overscanVertical * 2;
      }
    }
  }

  if(visible() && !fullScreen()) {
    uint widthMultiplier = layoutWidth / ceil(width);
    uint heightMultiplier = layoutHeight / height;
    uint multiplier = 3;//max(1, min(widthMultiplier, heightMultiplier));
    settings["View/Multiplier"].setValue(multiplier);
  }

  if(!emulator || !emulator->loaded()) return clearViewport();
  if(!video) return;

  uint viewportWidth;
  uint viewportHeight;
  if(settings["View/Output"].text() == "Center") {
    uint widthMultiplier = layoutWidth / ceil(width);
    uint heightMultiplier = layoutHeight / height;
    uint multiplier = min(widthMultiplier, heightMultiplier);
    viewportWidth = ceil(width * multiplier);
    viewportHeight = height * multiplier;
  } else if(settings["View/Output"].text() == "Scale") {
    double widthMultiplier = (double)layoutWidth / width;
    double heightMultiplier = (double)layoutHeight / height;
    double multiplier = min(widthMultiplier, heightMultiplier);
    viewportWidth = ceil(width * multiplier);
    viewportHeight = height * multiplier;
  } else if(settings["View/Output"].text() == "Stretch" || 1) {
    viewportWidth = layoutWidth;
    viewportHeight = layoutHeight;
  }

  //center viewport within viewportLayout by use of viewportLayout padding
  uint paddingWidth = layoutWidth - viewportWidth;
  uint paddingHeight = layoutHeight - viewportHeight;
  viewportLayout.setPadding({
    paddingWidth / 2, paddingHeight / 2,
    paddingWidth - paddingWidth / 2, paddingHeight - paddingHeight / 2
  });
}

auto Presentation::resizeWindow() -> void {
  if(fullScreen()) return;
  if(maximized()) setMaximized(false);

  double width = 1280.0 / 3.0;
  uint height = 240;
  uint multiplier = max(1, 3);  //settings["View/Multiplier"].natural()

  if(emulator) {
    auto display = emulator->display();
    width = display.width;
    height = display.height;
    if(settings["View/AspectCorrection"].boolean()) width *= display.aspectCorrection;
    if(!settings["View/Overscan"].boolean()) {
      if(display.type == higan::Interface::Display::Type::CRT) {
        uint overscanHorizontal = settings["View/Overscan/Horizontal"].natural();
        uint overscanVertical = settings["View/Overscan/Vertical"].natural();
        width -= overscanHorizontal * 2;
        height -= overscanVertical * 2;
      }
    }
  }

  setMinimumSize({ceil(width), height});
  setSize({ceil(width * multiplier), height * multiplier});
  layout.setGeometry(layout.geometry());
  resizeViewport();
}

auto Presentation::toggleFullScreen() -> void {
  if(!fullScreen()) {
    setFullScreen(true);
    video->setExclusive(settings["Video/Fullscreen/Exclusive"].boolean());
    if(video->exclusive()) setVisible(false);
    if(!input->acquired()) input->acquire();
  } else {
    if(input->acquired()) input->release();
    if(video->exclusive()) setVisible(true);
    video->setExclusive(false);
    setFullScreen(false);
  }
  //hack: give window geometry time to update after toggling fullscreen and menu/status bars
  usleep(20 * 1000);
  Application::processEvents();
  resizeWindow();
}
