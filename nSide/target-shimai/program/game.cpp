auto Program::load() -> void {
  if(!gameQueue) return;

  string location = gameQueue.left();
  string extension = Location::suffix(location).trimLeft(".", 1L);

  for(auto& emulator : emulators) {
    auto information = emulator->information();
    if(information.extension == extension) return load(*emulator);
  }

  gameQueue.reset();
}

auto Program::load(higan::Interface& interface) -> void {
  unload();

  auto information = interface.information();
  gamePaths.append(locateSystem({information.name, ".sys/"}));

  inputManager->bind(emulator = &interface);

  if(auto properties = string::read({gamePaths[0], "properties.bml"})) {
    emulator->properties().unserialize(properties);
  }

  if(auto options = string::read({gamePaths[0], "options.bml"})) {
    emulator->options().unserialize(options);
    emulator->setOption("hack/ppu/fast", true);
    emulator->setOption("hack/dsp/fast", true);
    emulator->setOption("hack/coprocessor/fast", true);
  }

  if(!emulator->load()) {
    emulator = nullptr;
    gamePaths.reset();
    return;
  }
  emulator->power();
  emulator->setOption("video/colorBleed", settings["Video/BlurEmulation"].boolean());
  emulator->setOption("video/interframeBlending", settings["Video/BlurEmulation"].boolean());
  emulator->setOption("video/colorEmulation", settings["Video/ColorEmulation"].boolean());
  emulator->setOption("video/scanlineEmulation", settings["Video/ScanlineEmulation"].boolean());
  updateAudioDriver();
  updateAudioEffects();

  presentation->resizeViewport();
  presentation->setTitle(emulator->titles().merge(" + "));
}

auto Program::unload() -> void {
  if(!emulator) return;

  presentation->clearViewport();
  emulator->unload();
  emulator = nullptr;
  gamePaths.reset();

  presentation->resizeViewport();
  presentation->setTitle("shimai");
  home->reset();
}
