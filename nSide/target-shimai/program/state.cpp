auto Program::stateName(uint slot) -> string {
  return {
    gamePaths(1), isNS() ? "nSide" : "higan", "/states/quick/",
    "slot-", slot, ".bst"
  };
}

auto Program::loadState(uint slot) -> bool {
  if(!emulator) return false;
  auto location = stateName(slot);
  auto memory = file::read(location);
  if(!memory) return false;
  serializer s(memory.data(), memory.size());
  if(!emulator->unserialize(s)) return false;
  return true;
}

auto Program::saveState(uint slot) -> bool {
  if(!emulator) return false;
  auto location = stateName(slot);
  serializer s = emulator->serialize();
  if(!s) return false;
  directory::create(Location::path(location));
  if(!file::write(location, {s.data(), s.size()})) return false;
  return true;
}
