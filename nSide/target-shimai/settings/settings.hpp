struct UserInterfaceSettings : Markup::Node {
  UserInterfaceSettings();
  auto save() -> void;
};

extern UserInterfaceSettings settings;
