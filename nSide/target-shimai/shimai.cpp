#include "shimai.hpp"
unique_pointer<Video> video;
unique_pointer<Audio> audio;
unique_pointer<Input> input;
higan::Interface* emulator = nullptr;

auto locate(string name) -> string {
  string location = {Path::program(), name};
  if(inode::exists(location)) return location;

  location = {Path::userData(), "nSide/", name};
  if(inode::exists(location)) return location;

  location = {Path::userData(), "higan/", name};
  if(inode::exists(location)) {
    if(Location::suffix(location) == ".bml" && Location::suffix(Location::dir(location)) != ".sys") {
      directory::create({Path::userData(), "nSide/"});
      file::copy(location, {Path::userData(), "nSide/", name});
      return {Path::userData(), "nSide/", name};
    }
    return location;
  }

  directory::create({Path::userData(), "nSide/"});
  return {Path::userData(), "nSide/", name};
}

auto locateSystem(string name) -> string {
  string location = {settings["Library/Location"].text(), "systems/", name};
  if(inode::exists(location)) return location;

  return locate(name);
}

auto hiro::initialize() -> void {
  Application::setName("shimai");
  Application::setScreenSaver(false);
}

#include <nall/main.hpp>
auto nall::main(Arguments arguments) -> void {
  new Program(arguments);
  Application::run();
}
