#pragma once
#include <higan/emulator/resource/resource.hpp>
#include <higan/emulator/emulator.hpp>

namespace nSide {
  //higan's properties are in the higan:: namespace.
  //Most notably, the license is GPLv3.

  static const vector<string> Contributors = {
    "Alyosha_TAS (Atari 2600 PIA, TIA, timing details)",
    //"Ange Albertini (higan logo)",  //not used in nSide
    "AWJ (MMC5 CHR banking)",
    "Bisqwit (Famicom PPU color generation for NTSC systems)",
    "blargg (Famicom testing)",
    "Braintrash (Locale for Français)",
    "_Demo_",
    "Derrick Sobodash",
    "DMV27",
    "Dr. Decapitator (DSP-1,2,3,4, ST-010,011 ROM dumps)",
    "endrift (Game Boy, Game Boy Advance fixes)",
    "FirebrandX",
    "FitzRoy (Famicom bug-testing)",
    "gekkio (Game Boy STAT IRQ and other fixes)",
    "GIGO",
    "jchadwick (Game Boy Advance fixes)",
    "Jonas Quinn (Game Boy, Game Boy Advance fixes)",
    "kevtris (Famicom PPU voltage levels)",
    "kode54",
    "krom",
    "LostTemplar (ST-018 program ROM analysis)",
    "Matthew Callis",
    "MerryMage (nall: elliptic curve cryptography)",
    "Nach (libco: setjmp jmpbuf support)",
    "NTI Productions (Locale for Português do Brasil)",
    "OV2 (ruby: XAudio2 support)",
    "Overload (Cx4 data ROM dump)",
    "RedDwarf",
    "Richard Bannister",
    "Ryphecha (Famicom APU emulation)",
    "segher (Cx4 reverse engineering)",
    "Talarubi (ST-018 discoveries and bug fixes, Game Boy Advance emulation, Mega Drive sound)",
    "Tauwasser (Game Boy DMA timing)",
    "tetsuo55",
    "TRAC",
    "trap15 (WonderSwan information and fixes)",
    "wareya (WASAPI driver)",
    "zones",
    "loopy (No longer used: MMC5 CHR banking)",
    "Ryphecha (No longer used: polyphase audio resampler)",
  };
  static const string Name    = "nSide";
  static const string Version = "009.16";
  static const string Author  = "hex_usr";
  static const string Website = "";

  //Incremented only when serialization format changes.
  //Only affects systems that were changed significantly in nSide compared to
  //higan. All other systems use higan::SerializerVersion instead.
  static const string SerializerVersion = "009.16(106.84)";
}

#include "debugger.hpp"
