#pragma once

namespace higan {

#if defined(DEBUGGER)
  #define debug(id, ...) if(debugger.id) debugger.id(__VA_ARGS__)
#else
  #define debug(id, ...)
#endif

}
