namespace Resource {
namespace Sprite {
extern const unsigned char CrosshairTurbo[322];
extern const unsigned char CrosshairRedSmall[175];
extern const unsigned char CrosshairGreenSmall[170];
extern const unsigned char CrosshairBlueSmall[175];
extern const unsigned char CrosshairTurboSmall[188];
extern const unsigned char FamicomBox1[183];
extern const unsigned char FamicomBoxOff[163];
extern const unsigned char FamicomBoxOn[174];
extern const unsigned char FamicomBox2[167];
extern const unsigned char FamicomBox3[168];
extern const unsigned char FamicomBox4[182];
}
}
