#include <higan/component/processor/mos6502/algorithms.cpp>

auto MOS6502::algorithmALR(uint8 i) -> uint8 {
  return algorithmLSR(algorithmAND(i));
}

auto MOS6502::algorithmANC(uint8 i) -> uint8 {
  uint8 o = algorithmAND(i);
  C = N;
  return o;
}

auto MOS6502::algorithmARR(uint8 i) -> uint8 {
  uint8 o = A & i;
  o = C << 7 | o >> 1;
  C = o.bit(6);
  Z = o == 0;
  V = o.bit(6) ^ o.bit(5);
  N = o.bit(7);
  return o;
}

auto MOS6502::algorithmAXS(uint8 i) -> uint8 {
  uint9 o = (A & X) - i;
  C = !o.bit(8);
  Z = uint8(o) == 0;
  N = o.bit(7);
  return o;
}

auto MOS6502::algorithmDCP(uint8 i) -> uint8 {
  uint8 o = algorithmDEC(i);
  A = algorithmCMP(o);
  return o;
}

auto MOS6502::algorithmISC(uint8 i) -> uint8 {
  uint8 o = algorithmINC(i);
  A = algorithmSBC(o);
  return o;
}

auto MOS6502::algorithmRLA(uint8 i) -> uint8 {
  uint8 o = algorithmROL(i);
  A = algorithmAND(o);
  return o;
}

auto MOS6502::algorithmRRA(uint8 i) -> uint8 {
  uint8 o = algorithmROR(i);
  A = algorithmADC(o);
  return o;
}

auto MOS6502::algorithmSLO(uint8 i) -> uint8 {
  uint8 o = algorithmASL(i);
  A = algorithmORA(o);
  return o;
}

auto MOS6502::algorithmSRE(uint8 i) -> uint8 {
  uint8 o = algorithmLSR(i);
  A = algorithmEOR(o);
  return o;
}

auto MOS6502::algorithmXAA(uint8 i) -> uint8 {
  //XAA/ANE should be used with an argument of 0x00 (xaa #$00) or when the
  //accumulator is 0xff prior to execution (lda #$ff...xaa #$??). All other
  //combinations are subject to corruption.
  //http://csdb.dk/release/?id=143981

  return (A | xaaNoise) & X & i;
}
